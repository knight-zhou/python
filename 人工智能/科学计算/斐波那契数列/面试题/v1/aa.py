#coding:utf-8
"""
aa=[0,1,1,2,3,5,8....]
要求输入索引即可得到相应的值
"""
# 递归方法
def res(n):
    assert n>=0, "n>0"
    if n<=1:
        return n

    return res(n-1) + res(n-2)

# for i in range(1,20):
#     print(res(i),end=' ')

print(res(6))