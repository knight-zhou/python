﻿#/usr/bin/env python
#coding:utf-8

def xx(n):
   """递归函数
   输出斐波那契数列"""
   if n <= 1:
       return n
   else:
       return(xx(n-1) + xx(n-2))

nterms  = 6  #输出几项在这里定义 

for i in range(nterms):     
    print(xx(i))


