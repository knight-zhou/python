﻿#!/usr/bin/env python
#coding:utf8

def xx(n):
   if n <= 1:  
       return n
   else:
       return(xx(n-1) + xx(n-2))

nterms  = 10  #输出几项在这里定义 
y = 5    #指定从哪一项开始,斐波那契数列,因为是不变的的序列
for i in range(y-1,nterms):     
    print(xx(i))