## python爬虫一般使用scrapy框架
* [中文翻译文档](http://scrapy-chs.readthedocs.io/zh_CN/0.24/intro/overview.html)
* [官方文档](https://doc.scrapy.org/en/latest/intro/tutorial.html)

### Scrapy命令交互模式
```
scrapy shell 网址(不需要引号)
request 对网址发起请求的请求信息
response 网址服务器响应请求 发回的响应信息
view(response) 调用系统自带的浏览器 查看response中保存着从网址获取的网页数据
fetch(url) 在交互模式下 重新对一个url网址发送请求 自动更新到request和response中

```
## Scrapy项目爬虫文件说明
* init.py  保持默认 不需要做任何修改
* items.py 自定义累的地方 也就是爬虫获取到数据之后 传入管道文件(pipelines.py)的载体
* pipelines.py  项目管道文件  对传入的项目类在红的数据进行一个清理和入库
* setting.py  Scrapy项目的设置文件 例如下载延迟 项目管道文件中类的启用以及自定义中间件的启用和顺序
* spiders目录  里面只有一个init.py文件 在该目录下定义爬虫类并继承scrapy.Spider 
* middlewares.py 中间件配置文件