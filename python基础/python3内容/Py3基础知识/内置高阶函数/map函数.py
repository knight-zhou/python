#coding:utf-8
# map将传入的函数依次作用到序列的每个元素，并结果返回,以下函数是求平方

def f(x):
    return x*x

r = map(f,[1,3,5,7])
print(list(r))
