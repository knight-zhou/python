#coding:utf-8
# reduce 可以把一个函数作用在一个序列上
from functools import reduce
def add(x,y):
    return x+y

print(reduce(add,[1,4,6]))
