def ds(x):
	return 2 * x + 1
print(ds(5))

#匿名函数
g = lambda x: 2 * x + 1   #g是函数名,x是传入的参数名
print(g(5))
