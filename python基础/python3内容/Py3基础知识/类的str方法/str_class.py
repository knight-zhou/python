# coding:utf-8
class Person(object):
    def __init__(self,name,sex):
        self.name=name
        self.sex=sex

    def __str__(self):
	#def __repr__(self):
        return "Person:{0},{1}".format(self.name,self.sex)


p = Person("knight",'man')
print (p)
