# coding:utf-8
import os
import time
from multiprocessing import Process

#daemon一定要在p.start()前设置,设置p为守护进程,禁止p创建子进程,并且父进程代码执行结束,p即终止运行
"""
主进程创建守护进程
　其一：守护进程会在主进程代码执行结束后就终止
　其二：守护进程内无法再开启子进程,否则抛出异常：
        AssertionError: daemonic processes are not allowed to have children
"""

def childfunc():
    print("子进程开始....")
    while True:
        # print("这是子进程....---业务功能函数...")
        pass


if __name__ == '__main__':
    p = Process(target=childfunc)
    p.daemon
    p.start()