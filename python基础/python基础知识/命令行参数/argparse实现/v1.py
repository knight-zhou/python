# coding:utf-8
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-l","--list", help="list jar process")
parser.add_argument("-n","--name", help="java name")
args = parser.parse_args()


if args.list:
    print(args.list)
if args.name:
    print(args.name)
if args.list and args.name:
    print(args.list+args.name)