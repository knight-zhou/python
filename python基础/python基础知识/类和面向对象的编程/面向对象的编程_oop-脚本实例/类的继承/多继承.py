#coding:utf-8
# 多继承, 可以继承多个类的属性和方法,使用继承减少了代码的复用,类的概念更加符合人类认识世界和改造世界的思维习惯
__metaclass__ = type

class Person:
	def eye(self):
		print "two eyes"
	def breast(self):
		print "有胸...."

class Girl:
	age = 28
	def color(self):
		print "The girl is white..."

class HotGirl(Person,Girl):
	''' 辣妹 '''
	pass

if __name__=="__main__":
	kong = HotGirl()
	kong.eye()
	kong.breast()
	kong.color()
	print kong.age