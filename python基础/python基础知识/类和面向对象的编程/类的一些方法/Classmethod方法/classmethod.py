#!/usr/bin/env python
#coding:utf8
"""
如果在@staticmethod中要调用到这个类的一些属性方法，只能直接类名.属性名或类名.方法名。
而@classmethod因为持有cls参数，可以来调用类的属性，类的方法，实例化对象等.
"""


class A():
	bar = "xxoo"
	def foo(self):      #类中的普通函数
		print "foo....common function"

	@staticmethod
	def static_foo():
		print "this is static_foo function"
		print "类中的变量: ",A.bar

	@classmethod
	def class_foo(cls):
		print "this is class_foo function"
		print cls.bar
		cls().foo()

# A.static_foo()   #不实例化类调用类中的静态方法
A.class_foo()    #不实例化调用类中的类方法