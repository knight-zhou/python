#!/usr/bin/env python
#coding:utf8
#使用静态方法可以不实例化类,而且能调用类中的方法

class people():
    name = "knight"
    age = "28"

    @staticmethod
    def a():
        print ("Yourname is:" + people.name + " You age is:" + people.age )

    @staticmethod
    def b():
        print ("yourname is: %s,your age is: %s" %(people.name,people.age))

people.a()
people.b()