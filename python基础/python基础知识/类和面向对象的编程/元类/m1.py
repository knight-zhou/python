#coding:utf-8
# ## py中一切皆对象,类可以创建实例对象,类本身也是对象
class C(object):
    pass

c = C()  # 实例化

print (c.__class__)   # 告诉某个实例对象c 是哪个类(和通过type函数是等同的)
print (C.__class__)   # 和通过type函数是等同的
print "--------------------"
#由于类也是对象，所以就可以在运行时动态的创建类，那么这时候就要用到内建函数type了

print (type(c))  # 和上面的结果一样
print (type(C))   # 和上面的结果一样
print "-------------------"
##  我们再看看使用type创建类的例子

def xxoo(self):
    print "{0} is {1} years old".format(self.name,self.age)

S = type(
    "Student", (object, ), {"name": "knight", "age": 28, "hello": xxoo}
    )

print type(S)
s = S()
print type(s)

#通过print输出，我们发现通过type动态的创建了一个Studnent类，并且通过这个类可以创建实例
# 函数type实际上是一个元类，元类就是用来创建类的"模板"。
# 我们可以通过类"模板"创建实例对象，同样，也可以使用元类"模板"来创建类对象；也就是说，元类就是类的类