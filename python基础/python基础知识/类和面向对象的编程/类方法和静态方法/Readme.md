## `@staticmethod` : 静态方法
经常有一些跟类有关系的功能但在运行时又不需要实例和类参与的情况下需要用到静态方法.比如更改环境变量或者修改其他类的属性等能用到静态方法. 这种情况可以直接用函数解决,

(1) 类内部使用

(2)写在类的定义里，@staticmethod写在函数正上方

(3) 参数的第一个参数不是self

(4)通过类名字调用

python中的@staticmethod 主要是方便将外部函数集成到类体中,美化代码结构,重点在不需要类实例化的情况下调用方法.

```
#!/usr/bin/env python
#coding:utf8
#使用静态方法可以不实例化类,而且能调用类中的方法

class people():
    name = "knight"
    age = "28"

    @staticmethod
    def a():
        print ("Yourname is:" + people.name + " You age is:" + people.age )

    @staticmethod
    def b():
        print ("yourname is: %s,your age is: %s" %(people.name,people.age))



people.a()
people.b()

```
## `@classmethod` : 类方法

相同点: 类方法和静态方法的调用一样，都是通过类就可以直接调用。

不同点：类方法，需要传入该类，定义类方法的时候要传一个默认的参数cls。静态方法则不用。

```
#!/usr/bin/env python
# coding: utf-8

class A(object):  
    bar = 1  
    def foo(self):  
        print 'foo'  
 
    @staticmethod  
    def static_foo():  
        print 'static_foo'  
        print A.bar  
 
    @classmethod  
    def class_foo(cls):  
        print 'class_foo'  
        print cls.bar  
        cls().foo()  
  
A.static_foo()  
A.class_foo()
```