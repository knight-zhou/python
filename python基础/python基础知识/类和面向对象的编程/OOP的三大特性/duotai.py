#coding:utf8
#以下是多态，不同类的同一个方法
class A:
	def fun(self):
		print ("I am a")

class B:
	def fun(self):
		print ("I am b")

a=A()
b=B()

a.fun()
b.fun()
