## py2 有时候会遇到ascii码，报错如下：
```
UnicodeEncodeError: 'ascii' codec can't encode characters in position 0-1: ordinal not in range(128)
```
解决:
```
f='\u53eb\u6211'
print f
print(f.decode('unicode-escape')) 

```