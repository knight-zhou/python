#coding:utf8
'''
Template无疑是一个好东西，可以将字符串的格式固定下来，重复利用。
从而减轻代码量
#Template中有两个重要的方法：substitute和safe_substitute
这两个方法都可以通过获取参数返回字符串
'''


from string import Template
s =  Template('$a and $b are friends!')
print s.substitute(a='you',b='me')
print  s.safe_substitute(a="apple",b="banbana")

#通过获取字典直接传递数据
d = {'a':'knight','b':'Tom'}
print s.substitute(d)