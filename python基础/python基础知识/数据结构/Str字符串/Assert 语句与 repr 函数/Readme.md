repr函数:
	如果我们想让任意一个值转换为一个字符串，我们可以将这个值传入Repr函数，那么Repr函数就会将这个值
	转换为字符串,然后返回

Assert函数:
	有时候 我们确信某个表达式的值为真,那么此时我们想要检验一下,我们可以使用Assert语句对这个表达式进行声明
	假如为真,正确执行,假如为假,则引发AssertionError这个错误
	