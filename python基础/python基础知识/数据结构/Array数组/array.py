#coding:utf8
"""
数组并不是Python中内置的标配数据结构,和list有一些类似
The array module provides an array() object that is
like a list that stores only homogeneous（相同的） data and stores it more compactly(简洁).


"""
from array import array
import binascii

b = [5,7,8,9,10]
b1 = "thi is array" 
# s = 'This is the array'
a = array('H', b)  # H表示二进制而不是常规的16字节
a1 = array('c',b1)   #字符串类型使用c模式

print "字符串数组:",a1[0]

print "打印数组:",a[0]
print "打印数组元素：",a[2:3]