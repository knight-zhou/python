#!/usr/bin/env python
#coding:utf-8
import socket
server = socket.socket() #获得socket实例
server.bind(("localhost",9998)) #绑定ip port
server.listen(100)  #开始监听
print("等待客户端的连接...")
conn,addr = server.accept() #接受并建立与客户端的连接,程序在此处开始阻塞,只到有客户端连接进来...
print("新连接:",addr )
while True:
    data = conn.recv(1024)
    if not data:                 # 如果数据为空防止进入死循环
        print ("client have break")
        break

    print("收到消息:",data)
    conn.send(data.upper())
server.close()
