#coding:utf-8
"""
python允许程序员自定义异常，用于描述python中没有涉及的异常情况，
自定义异常必须继承Exception类，自定义异常按照命名规范以"Error"结尾，显示地告诉程序员这是异常
"""

class KnightException(Exception):
	def __init__(self):
		print ("这是个自定义的knight异常,使用raise 引发异常之后的语句不会执行.....")

try:
	s = None
	if s is None:
		print ("s 是一个空对象")
		raise KnightException
	print (len(s))
except KnightException:
	print ("空对象没有长度....")
