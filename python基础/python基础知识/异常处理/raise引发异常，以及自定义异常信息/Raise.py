#coding:utf-8
#使用raise引发异常,NameError是内置异常类(如果你定义为Error会提示没有定义)
#  如果引发NameError异常，后面的代码将不能执行
try:
	s = None
	if s is None:
		print "s 是空对象...."
		# raise  NameError         
	print len(s)
except TypeError:
	print "空对象没有长度..."