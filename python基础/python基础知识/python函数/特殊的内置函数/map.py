#!/usr/bin/env python
#coding:utf-8
#内置的高阶函数，它接收一个函数 f 和一个 list，并通过把函数 f 依次作用在 list 的每个元素上，得到一个新的 list 并返回。

def aa(x):
	return x*x*x

print map(aa,[3,6,9])
