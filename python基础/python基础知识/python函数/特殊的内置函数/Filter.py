#coding:utf8
#filter（）函数包括两个参数，分别是function和list。
#该函数根据function参数返回的结果是否为真来过滤list参数中的项，最后返回一个新列表.

def f1(s):
	return s if s!='a' else None

ret = filter(f1,['a','b','c'])
print ret
