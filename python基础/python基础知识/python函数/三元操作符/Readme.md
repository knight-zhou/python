三元运算符通常在Python里被称为条件表达式,这些表达式基于真(true)/假(false)的条件判断

它允许用简单的一行快速判断,而不是使用复杂的多行if语句.这在大多数时候非常有用,而且可以使代码简单可维护。

不使用三元表达式:
```python
#coding:utf8
x,y = 4,5
print x
print y
if x < y:
        small = x
else:
        small = y
print "the samll num is :",small
```
使用：
```python
#coding:utf8
x = 1
y = 2
b = x if x < y else y
```
