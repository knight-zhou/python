#coding:utf8
"""
__new__():

__new__()在__init__()之前被调用，
用于生成实例对象.
"""
class A(object):         #继承 object新式类才有__new__
    def __init__(self):
        print "init...."
    def __new__(cls,*args, **kwargs):  #至少要有一个参数cls
        print "new %s"%cls
        return object.__new__(cls, *args, **kwargs)
 
A()

# print "\033[33m -----这是分割线---- \033[0m"
print "-------------------------这是分割线-------------------------------"

"""
知识点：

继承自object的新式类才有__new__
__new__至少要有一个参数cls，代表要实例化的类，此参数在实例化时由Python解释器自动提供
__new__必须要有返回值，返回实例化出来的实例，这点在自己实现__new__时要特别注意，可以return父类__new__出来的实例，或者直接是object的__new__出来的实例
__init__有一个参数self，就是这个__new__返回的实例，__init__在__new__的基础上可以完成一些其它初始化的动作，__init__不需要返回值
"""

# 若__new__没有正确返回当前类cls的实例，那__init__是不会被调用的，即使是父类的实例也不行
# 
class A(object):
    pass
 
class B(A):
    def __init__(self):
        print "init"
    def __new__(cls,*args, **kwargs):
        print "new %s"%cls
        return object.__new__(A, *args, **kwargs)
 
b=B()
print type(b)