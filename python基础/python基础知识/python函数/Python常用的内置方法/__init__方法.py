#coding:utf8
"""

__init__():
__init__方法在类的一个对象被建立时，
马上运行。这个方法可以用来对你的对象做一些你希望的初始化。
这个名称的开始和结尾都是双下划线,也叫作初始化方法。

"""
class Person:
	"""
说明：__init__方法定义为取一个参数name（以及普通的参数self）。
在这个__init__里，我们只是创建一个新的域，也称为name。
注意它们是两个不同的变量，尽管它们有相同的名字。点号使我们能够区分它们。
最重要的是，我们没有专门调用__init__方法，只是在创建一个类的新实例的时候，
把参数包括在圆括号内跟在类名后面，从而传递给__init__方法。这是这种方法的重要之处。
现在，我们能够在我们的方法中使用self.name域。这在sayHi方法中得到了验证。

	"""
	def __init__(self,name):
		self.name = name
	def sayHi(self):
		print "Hello, my name is",self.name

p = Person('knight')
p.sayHi()

print "doc文档如下:",p.__doc__