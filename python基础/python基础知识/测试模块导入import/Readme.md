## 一、同级目录下的调有
程序结构如下：
```
-- src
    |-- mod1.py
    |-- test1.py
```
若在程序test1.py中导入模块mod1, 则直接使用

import mod1

或

from mod1 import *;

## 二、调用子目录下的模块

程序结构如下：
```
-- src
    |-- mod1.py
    |-- lib
    |    |-- mod2.py
    |-- test1.py
```

这时看到test1.py和lib目录（即mod2.py的父级目录），如果想在程序test1.py中导入模块mod2.py ，可以在lib件夹中建立空文件__init__.py文件(也可以在该文件中自定义输出模块接口)，然后使用：

from lib.mod2 import *

或

import lib.mod2.

## 三、调用上级目录下的文件

程序结构如下：
```
-- src
    |-- mod1.py
    |-- lib
    |    |-- mod2.py
    |-- sub
    |    |-- test2.py

```
这里想要实现test2.py调用mod1.py和mod2.py ，做法是我们先跳到src目录下面，直接可以调用mod1，然后在lib上当下建一个空文件__init__.py ，就可以像第二步调用子目录下的模块一样，

通过import  lib.mod2进行调用了。具体代码如下：
```
import sys
sys.path.append("..")
import mod1
import mod2.mod2
```

#### 导入和go 以及java的对比
* python的导入和文件名有关

* go的导入和文件名无关,公共和私有和首字母大小写有关

* java 在同一个文件夹下会自动发现,IDE工具会提示你如何导入