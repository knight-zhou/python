#coding:utf-8
import threading
from time import ctime

class MyThread(threading.Thread):
    def __init__(self, func, args, name):
        threading.Thread.__init__(self)
        self.name = name
        self.func = func
        self.args = args

    def run(self):
        print ('starting', self.name, 'at:',ctime())
        apply(self.func, self.args)
        print (self.name, 'finished at:', ctime())

# 计算数字
def fun1(x):
    y = 0
    for i in range(x):
        y+=1

# 计算数字
def fun2(x):
    y = 0
    for i in range(x):
        y+=1
        
def main():
    print ('staring single thread at:',ctime())   # 单线程开始
    fun1(10000000)
    fun2(10000000)
    print ('finished single thread at:',ctime())  # 单线程结束

# 多线程处理
    t1 = MyThread(fun1,(10000000,),fun1.__name__)
    t2 = MyThread(fun2,(10000000,),fun2.__name__)
    t1.start()    # 多线程 t1 开始
    t2.start()     # 多线程 t2 开始
    t1.join()
    t2.join()
    
    print ('all done.........')
    
if __name__ == '__main__':
    main()

'''
结论:
结果显示对于同样的问题多线程耗费了多一倍的时间，fun1,、fun2都是计算型程序(计算数字)
这就意味着两个代码都需要占用CPU资源，虽然采用了多线 程但CPU资源是唯一的（不考虑多CPU多核的情况）
同一时刻只能一个线程使用，导致多线程无法真正的并发，相反由于线程的切换的开销，效率反而有明显 的下降。
由此看以在单CPU的场景下对于计算密集型的程序，多线程并不能带来效率的提升
'''