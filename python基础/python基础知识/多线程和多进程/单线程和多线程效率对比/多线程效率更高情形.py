#coding:utf-8
import threading
from time import ctime

class MyThread(threading.Thread):
    def __init__(self, func, args, name):
        threading.Thread.__init__(self)
        self.name = name
        self.func = func
        self.args = args

    def run(self):
        print ('starting', self.name, 'at:',ctime())
        apply(self.func, self.args)
        print (self.name, 'finished at:', ctime())


## 写入一定数量的文件
def fun1(x):
    for i in range(x):
        fd = open('1','w')
        fd.close()

def fun2(x):
    y = 0
    for i in range(x):
        y+=1
        
def main():
    print ('staring single thread at:',ctime())  # 单线程开始
    fun1(15000)
    fun2(10000000)
    print ('finished single thread at:',ctime())  # 单线程结束


## 多线程处理
    t1 = MyThread(fun1,(15000,),fun1.__name__)
    t2 = MyThread(fun2,(10000000,),fun2.__name__)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    
    print ('all finished.......')   
    
if __name__ == '__main__':
    main()

'''
结论:
结果显示这个程序采用多线程比单线程的效率有明显的提升。
这是由于fun1主要是文件的操作
fun2是计算操作
两个函数是在做不同的事情(类似于一边聊qq一边看电影可以并发执行 并不干扰)
单线程的情况下，虽然两个程序主要使用不同的资源但是在线程内部只能串行执行
在IO操作的时候，CPU实际是无事可做。
多线程的情况下，如果一个线程在等待IO操作，线程会马上调度到 另外一个线程上，并发的使用了不同的资源。
'''
