# coding:utf-8
import threading
from time import sleep,ctime

loops = [1,2]

def loop(nloop,nsec):
    print ("开始执行函数",nloop,"at:",ctime())
    sleep(nsec)
    print ("函数",nloop,'结束于:',ctime())

def main():
    print ("开始计时:",ctime())
    threads = []     # 定义空列表
    nloops = range(len(loops))       # 指定开启多多线程的个数
    #完成所有线程分配，并不立即开始执行
    for i in nloops:
        t = threading.Thread(target=loop,args=(i,loops[i]))    # Thread 实例传给他一个函数 实现多线程
        threads.append(t)    # 并追加到theads的列表
    #开始调用start方法，同时开始所有线程
    for i in nloops:
        threads[i].start()
    #join方法：主线程等待所有子线程执行完成，再执行主线程接下来的操作。
    for i in nloops:
        threads[i].join()

    print ("所有的都完成的时间:",ctime())
if __name__=="__main__":
    main()