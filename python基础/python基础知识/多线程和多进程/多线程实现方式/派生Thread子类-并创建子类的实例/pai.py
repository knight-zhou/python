import threading
import time

class myThread(threading.Thread):  # 继承父类threading.Thread
    def __init__(self,name, counter):
        threading.Thread.__init__(self)
        self.name = name
        self.counter = counter

    def run(self):  # 把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
        print("Starting " + self.name)
        print("Exiting " + self.name)

# 创建新线程
thread1 = myThread( "Thread-1", 1)
thread2 = myThread("Thread-2", 2)

# 开启线程
thread1.start()
thread2.start()

print("Exiting Main Thread")