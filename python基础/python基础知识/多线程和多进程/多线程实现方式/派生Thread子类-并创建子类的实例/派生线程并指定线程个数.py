import threading
import time

class myThread(threading.Thread):  # 继承父类threading.Thread
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):  # 把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
        print("Starting--- ")
        print("Exiting--")

# 创建新线程,并指定线程个数
threads_list=[]
for i in range(5):
    threads_list.append(myThread())

# 开启线程
for t in threads_list:
    t.start()


print("Exiting Main Thread")