#### 多线程和多进程
### (1)今天面试，面试官问我 多线程和多进程做一件事情谁的效率更高和时间更短？

正确回答:这两个没有可比性,因为一个程序可以是多个进程 也有可能是单个进程。一个进程可以多个线程

也可以单个线程。 这如何比较？ 这样的比较就相当于问 足球运动员和篮球远动员谁跑的更快的道理一样.

#### (2)那问题来了，单个线程和多个线程 做时间谁的效率更快？

正确回答: 在实际的操作中往往遇到性能的问题，都尝试使用多线程来解决问题，但多线程程序并不是在任何情况下都能提升效率.

在一些情况下恰恰相反，反而会降低程序的性能. 举例请看本目录下的两个文件

#### 结论
线程本身由于创建和切换的开销，采用多线程不会提高程序的执行速度，反而会降低速度，但是对于频繁IO操作的程序，多线程可以有效的并发.

对于包含不同任务的程序,可以采用多线程 那样会提高并发速度

在实际的开发中对于性能优化的问题需要考虑到具体的场景来考虑是否使用多线程技术.

#### 并行与并发
### 并发
当有多个线程在操作时,如果系统只有一个CPU,则它根本不可能真正同时进行一个以上的线程，它只能把CPU运行时间划分成若干个时间段,再将时间 段分配给各个线程执行，在一个时间段的线程代码运行时，其它线程处于挂起状。

这种方式我们称之为并发(Concurrent)。

#### 并行
当系统有一个以上CPU时,则线程的操作有可能非并发。当一个CPU执行一个线程时，另一个CPU可以执行另一个线程，两个线程互不抢占CPU资源，可以同时进行，这种方式我们称之为并行(Parallel)。

#### 区别
并发和并行是即相似又有区别的两个概念:
* 并发 是指两个或多个事件在同一时间间隔内发生

* 并行 是指两个或者多个事件在同一时刻发生；

#### python多线程的三种实现方式
* 创建 Thread 的实例，传给它一个函数(推荐)
* 派生 Thread 的子类，并创建子类的实例(推荐)
* 创建 Thread 的实例，传给它一个可调用的类实例(不推荐也不做介绍)




