#coding:utf8

import threading
from time import ctime,sleep


def music(func):
    for i in range(2):
        print ("I was listening to %s. %s" %(func,ctime()))
        sleep(1)

def move(func):
    for i in range(2):
        print ("I was at the %s! %s" %(func,ctime()))
        sleep(5)

threads = []
t1 = threading.Thread(target=music,args=(u'爱情买卖',))
threads.append(t1)

#接着以同样的方式创建线程t2，并把t2也装到threads数组。
t2 = threading.Thread(target=move,args=(u'阿凡达',))
threads.append(t2)


if __name__ == '__main__':
    t1.setDaemon(True)
    t2.setDaemon(True)
    t1.start()
    t2.start()


    print ("我都看完了 %s" %ctime())

