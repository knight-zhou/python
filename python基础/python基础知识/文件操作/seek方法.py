#coding:utf8
"""

概述
seek() 方法用于移动文件读取指针到指定位置。

seek() 方法语法如下：
fileObject.seek(offset[, whence])

参数
offset -- 开始的偏移量，也就是代表需要移动偏移的字节数
whence：可选，默认值为 0。给offset参数一个定义，表示要从哪个位置开始偏移；
0代表从文件开头开始算起，1代表从当前位置开始算起，2代表从文件末尾算起。

返回值
该函数没有返回值。

"""


f = open(r'file.txt','r')
# f.write("abcdef")

f.seek(2,1)
print f.read(1)

f.close()
