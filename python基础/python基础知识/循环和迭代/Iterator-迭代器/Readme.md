如果给定一个list或tuple，我们可以通过for循环来遍历这个list或tuple，这种遍历我们称为迭代（Iteration）。

语句在容器对象中调用 iter() 。 该函数返回一个定义了 next() 方法的迭代器对象，它在容器中逐一访问元素。没有后续的元素时，next() 抛出一个 StopIteration 异常.
