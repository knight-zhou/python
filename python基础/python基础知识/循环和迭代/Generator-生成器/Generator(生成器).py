#coding:utf8
L = [x * x for x in range(10)]
# print L

g = (x * x for x in range(10))

#创建L和g的区别仅在于最外层的[]和()，L是一个list，而g是一个generator。
#我们可以直接打印出list的每一个元素，但我们怎么打印出generator的每一个元素呢？

# 每次调用next()，就计算出下一个元素的值，直到计算到最后一个元素，没有更多的元素时，抛出StopIteration的错误。

# print g
# print g.next()  
# print g.next()

#generator也是可迭代对象,可以通过for循环进行迭代.
for n in g:
	print n