#coding:utf8
print range(5)
print range(0,6,2)

print "下面是xrange的用法:------------------"
print xrange(5)
print list(xrange(5))
print list(xrange(0,6,2))

print "看看两者的数据类型:------------------"
a = range(0,100)
print a[0],a[1]

#而xrange则不会直接生成一个list，而是每次调用返回其中的一个值：
#所以xrange做循环的性能比range好，尤其是返回很大的时候。尽量用xrange吧，除非你是要返回一个列表
print "range的数据类型是:",type(a)
b = xrange(0,100)
print type(b)
print b[0],b[1]
