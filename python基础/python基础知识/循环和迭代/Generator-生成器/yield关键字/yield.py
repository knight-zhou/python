#coding:utf8
def func():
	for i  in xrange(4):
		yield i                  # 用起来像return，yield在告诉程序，要求函数返回一个生成器


a = [i for i in xrange(10)]   #通过列表来创建生成器
# print a
print "----------分割线--------------"
#调用如下
f = func()
f 					# 此时生成器还没有运行
print f.next()      # 当i=0时,遇到yield关键字,直接返回
print f.next()		# 继续上一次执行的位置,进入下一层循环
print f.next()
print f.next()
# print f.next()    # 当执行完最后一次循环后,结束yield语句,生成StopIteration异常
