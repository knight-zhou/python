面向对象编程（Object Oriented Programming，OOP，面向对象程序设计）


## 装饰器的理解
文章： https://foofish.net/python-decorator.html

官方文档请查看:
 What's New in Python  然后搜索:decorator

装饰器(decorator)：

* 1.装饰器用来装饰函数
* 2.返回一个函数对象
* 3.被装饰的函数标识符指向返回的函数对象
* 4.语法糖 @deco

解释:装饰器（decorator）本身是一个函数，包装另一个函数或类，它可以让其他函数在不需要改动代码情况下动态增加功能，装饰器返回的也是一个函数对象。

请看本目录下的脚本进行理解