##### 什么是*args和**kwargs？
回答:这两个是python中的可变参数。

`*args`: 表示用来发送一个非键值对的可变数量的参数列表给一个函数(结果返回的是tuple)

`**kwargs`: 允许你将不定长度的键值对或者函数里处理带名字的参数作为参数传递给一个函数。(结果返回的是dict)

并且同时使用`*args`和`**kwargs`时，必须`*args`参数列要在`**kwargs`前


Arbitrary Argument官方文档:

Finally, the least frequently(频繁) used option(选项) is to specify that a function can be called with an arbitrary(任意的) number of arguments. These arguments will be wrapped（装饰） up in a tuple.

