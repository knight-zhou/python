#!/usr/bin/env python
#coding:utf8

#现在我们将args和kwargs混合使用,args必须在前面

def foo(*args,**kwargs):
	print "args 为:",args
	print "kwargs 为",kwargs
	print "--------------------------------"

if __name__ == '__main__':
	foo(1,2,3,name='knight',city='sz')
	# foo('x','y','z')
	# foo(name='xx',name2='oo')
	