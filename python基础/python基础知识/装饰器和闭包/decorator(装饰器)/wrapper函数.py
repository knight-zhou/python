#!/usr/bin/env python
#coding:utf8

#经过以上两个脚本，你很疑惑wrapper 是什么鬼？英文意思(wrapper:包装)我们把wrapper替换成其他来实验下：

def log(func):
    def xx(*args, **kw):
    # def wrapper(*args, **kw):
        print '打印日志.... %s():' % func.__name__
        return func(*args, **kw)
    return xx       #返回函数
    # return wrapper     

@log
def now():
	print '2016-01-05  定义原始的now函数'

now() 

print "替换之后能正常输出,说明wrapper 并没有其他含义，只是个函数名称而已"