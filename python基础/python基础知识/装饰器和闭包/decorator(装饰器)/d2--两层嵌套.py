#coding:utf8

"""
现在，假设我们要增强now()函数的功能，比如，在函数调用前后自动打印日志，但又不希望修改now()函数的定义，
这种在代码运行期间动态增加功能的方式，称之为“装饰器”（Decorator）。
本质上，decorator就是一个返回函数的高阶函数。
"""
#我们定义如下(两层嵌套)：
def log(func):
    def wrapper(*args, **kw):
        print '打印日志.... %s():' % func.__name__
        return func(*args, **kw)
    return wrapper       #返回函数

#观察上面的log，因为它是一个decorator，
#所以接受一个函数作为参数，并返回一个函数。我们要借助Python的@语法，把decorator置于函数的定义处：
@log
def now():
	print '2016-01-05  定义原始的now函数'

now() #调用now()函数，不仅会运行now()函数本身，还会在运行now()函数前打印一行日志：   

"""
由于log()是一个decorator(装饰器)，返回一个函数，所以，原来的now()函数仍然存在，只是现在同名的now变量指向了新的函数，
于是调用now()将执行新函数，即在log()函数中返回的wrapper()函数。

wrapper()函数的参数定义是(*args, **kw)，因此，wrapper()函数可以接受任意参数的调用。
在wrapper()函数内，首先打印日志，再紧接着调用原始函数。
"""