#!/usr/bin/env python
#coding:utf-8

from functools import wraps

# 装饰器实际上是一个函数
# 有两个特别之处
# 1. 参数是一个函数
# 2. 返回值是一个函数 (既是函数进夜市函数出)


# 在打印run之前 先要打印一个hello world
# 在所有的函数执行之前 都要打印一个hello world


def my_log(func):
    @wraps(func)

    # def wrapper():
    # 用位置参数实现，解决哟参数和没有参数的问题
    def wrapper(*args,**kwargs):
        print 'hello world'
        # func()
        func(*args,**kwargs)

    return wrapper    #去掉括号，表示函数体，不执行函数


#run = my_log(run) = wrapper
#run()

@my_log
def run():
    print 'run'


# run.__name__代表的是run的这个函数的名称


# 你想在add中也打印hello  world！,那么每个函数前面要加个 print "hello world"
# 如果产品经理对你讲 我不要打印"hello world" 了，我真想打印hello,那不是每个函数要改 print
# 所以使用装饰器来实现


#现在给add函数也加装饰器试一试,直接报错.
@my_log
def add(a,b):
    c = a + b
    print u'结果是:{0}'.format(c)

# 相当于以下这条
#add = my_log(add) = wrapper

# 现在已经给run添加了"hello world"了.

run()

#只执行这个参数是可行，但是run()就报错了
add(4,5)

#  发现装饰器把名字改掉了,加了@wraps(func) 就正常了
print "-"*10

print run.__name__

print "-"*10

## ok到此解决了
