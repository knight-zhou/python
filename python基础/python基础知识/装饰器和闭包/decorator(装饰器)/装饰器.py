#coding:utf8
#定义原始now函数,装饰器可以形象的理解为装饰原来的函数，不改变原来函数的代码

def now():
	print '2016-01-05'
f = now
f()

#函数对象有一个__name__属性,可以拿到名字:
print now.__name__   #获取属性名字
# print f.__name__      #获取属性名字

#现在，假设我们要增强now()函数的功能，比如，在函数调用前后自动打印日志，但又不希望修改now()函数的定义，代码请看: d2--两层嵌套.py
