#!/usr/bin/env python
#coding:utf-8
import logging
import logging.config

log_config = 'logconfig.ini'
logging.config.fileConfig(log_config,disable_existing_loggers=False)
logger = logging.getLogger(__name__)

logger.info("this is info message")