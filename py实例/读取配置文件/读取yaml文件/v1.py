# coding:utf-8
import yaml
import os
#获取当前文件夹的路径
curPath=os.path.dirname(os.path.realpath(__file__))
#获取yaml文件路径
yamlPath=os.path.join(curPath,"app.yaml")
f=open(yamlPath,'r',encoding='utf-8')
mm = yaml.load(f,Loader=yaml.FullLoader)   # 使用更加安全的load
print(mm)