#!/usr/bin/env python
#coding:utf8
import sys

num = len(sys.argv)

if num < 3:
    print "Usage:sh {0} modulename ENV".format(sys.argv[0])
    print "modulename:assets|video|orderPay|travel|stadium|activity|scheduler|club|place|authority|comment|user|sms|financialSystem|operative|remoteMiddleware"
    print "ENV:prod|pre"
else:
    if "pre" or "prod" not in sys.argv[2]:
        print "\033[31m  No arguments {0} !!  \033[0m".format(sys.argv[2])
