方法一、：
xargs < test.txt

方法二、整个文件读入一个变量，然后直接打印，也很容易理解：
a=`cat test.txt`;echo $a

方法三、使用tr把换行符替换成空格：
tr -s "\n" " " < test.txt;echo

方法四、使用sed，把整个文件读入保持空间，处理最后一行的时候，替换所有换行符为空格，打印：
sed -n '1h;1!H;${g;s/\n/ /g;p;}' test.txt

方法五、使用awk，读入一行打印一行，但是不打印换行符，最后一行多打印一个换行符：
awk '{printf("%s ",$0);}END{print}' test.txt

方法六、使用paste命令格式化打印，-d指定分隔符，-s表示合并成一行：
paste -d" " -s - < test.txt

方法七、使用pr格式化打印，-s指定分隔符，-50指定每行打印多少域，-t指定取消页眉、页尾：
pr -50t -s" " test.txt


用以上方法去读下面的文件老是行不通,找了半天原因，原来是字符格式问题，最后用dos2unix test.txt 解决
yzy112201@163.com
yzy112202@163.com
yzy112203@163.com
