## python:字符串转换成字节的三种方式
```
str='zifuchuang'

第一种 b'zifuchuang'
第二种bytes('zifuchuang',encoding='utf-8')
第三种('zifuchuang').encode('utf-8')
```
