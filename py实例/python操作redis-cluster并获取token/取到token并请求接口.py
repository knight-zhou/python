#!/usr/bin/env python
#coding:utf-8
import redis
from rediscluster import StrictRedisCluster
import sys,requests,os
import logging


#刷新token
requests.get('http://192.168.30.141:9000/referetoken')

redis_nodes =  [{'host':'192.168.30.210','port':6379},
                # {'host': '192.168.30.24', 'port': 6380},
                # {'host': '192.168.30.24', 'port': 6381},
                # {'host': '192.168.30.24', 'port': 6382},
                # {'host': '192.168.30.24', 'port': 6383},
                # {'host': '192.168.30.24', 'port': 6384}
                ]
try:
    redisconn = StrictRedisCluster(startup_nodes=redis_nodes)
except Exception,e:
       print "Connect Error!"
       sys.exit(1)

token_value = redisconn.get('finance:bill_api_token')
print "token value:{0}".format(token_value)

## 取到token之后的操作
fmt = '%(asctime)s - %(process)s - %(levelname)s:%(message)s'
logging.basicConfig(filename=os.path.join(os.getcwd(),'token1.log'),level = logging.INFO, filemode = 'a',format = fmt)

##执行接口
headers = {'token': token_value}
url='http://192.168.30.141:9000/bill/createbilldetail'
r = requests.get(url, headers=headers)

##记录日志
#logging.info(r.text)
logging.info(r)
