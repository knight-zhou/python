#!/usr/bin/env python
#coding:utf-8
import redis
from rediscluster import StrictRedisCluster
import sys,requests,os,time
import logging

requests.get('http://finance.quncaotech.com/referetoken;')
redis_nodes =  [{'host':'10.45.141.188','port':7000},
                ]
try:
    redisconn = StrictRedisCluster(startup_nodes=redis_nodes)
except Exception,e:
       print "Connect Error!"
       sys.exit(1)

token_value = redisconn.get('finance:bill_api_token')

headers = {'token': token_value}
#day
url='http://finance.quncaotech.com/bill/createbilldetail'
r = requests.get(url, headers=headers)
print r.text

time = time.strftime("%Y%m%d-%H%M")
fmt = '%(asctime)s - %(process)s - %(levelname)s:%(message)s'
logging.basicConfig(filename=os.path.join(os.getcwd(),'/opt/web/finance_task_log/{0}_token.log').format(time),level = logging.INFO, filemode = 'a',format = fmt)
logging.info(r.text)
