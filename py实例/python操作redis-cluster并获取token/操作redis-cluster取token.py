#!/usr/bin/env python
#coding:utf-8
import redis
from rediscluster import StrictRedisCluster
import sys

def redis_cluster():
    redis_nodes =  [{'host':'192.168.30.24','port':6379},
                    # {'host': '192.168.222.24', 'port': 6380},
                    # {'host': '192.168.222.24', 'port': 6384}
                   ]
    try:
        redisconn = StrictRedisCluster(startup_nodes=redis_nodes)
    except Exception,e:
        print "Connect Error!"
        sys.exit(1)

    # redisconn.set('name','knight xxoo')
    # redisconn.delete('finance:bill_api_token')
    # redisconn.set('age',18)
    # print "age  is: ", redisconn.get('age')
    print "token is : ", redisconn.get('finance:bill_api_token')
    print "name is : ", redisconn.get('name')

redis_cluster()