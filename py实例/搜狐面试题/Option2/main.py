#!/usr/bin/env python
#-*- coding:utf-8 -*-
#python 3.5

'''
主控模块
'''



from page_parser import PageParser
from multiprocessing import Pool
from data_base import DataOper


#数据库操作模块实例化
data = DataOper()  

#设置根url
root_url = 'http://m.sohu.com/'

def get_urls(url):
	global urls,counter
	try:
		#可选,进行抓取的url写入一个文件中,但会增加I/O操作
		# with open('url_list.txt','a') as test:
		# 	test.write(url + '\n')
		data.delete(url)
		print(url)
		analysis = PageParser(url)
		for i in analysis.get_urls():
			if data.check(i):
				data.delete(i)       #通过数据库模块的check函数判断，如果不是有效的url就删除数据
			else:
				data.insert(i)      #是有效函数就插入mongodb中
	except:
		pass

#先将根url放在数据库中
# data.insert(root_url)

d = data.find()    #数据中都是 有效的url
print (type(d))  # url放进列表
print ("list length: ",len(d))
print ("list first element: ",d[0])

#设置多进程进行抓取
# pool = Pool(processes=1)
# pool.map(get_urls,data.find())  # map函数，迭代list 进行参数传进来.(Pool是用来多进程操作用的)
# pool.close()
# pool.join()
print ("below------line-----")
get_urls(d[0])  #得到有效的url

