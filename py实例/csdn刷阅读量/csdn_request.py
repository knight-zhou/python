# coding:utf-8
import requests
import re
import time
payload = ""
# 请求头
headers = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
    "Cookie": "uuid_tt_dd=10_19477688970-1568173861503-355956; dc_session_id=10_1568173861503.962345; Hm_lvt_6bcd52f51e9b3dce32bec4a3997715ac=1575426861,1576639239,1577171192,1577416227; Hm_ct_6bcd52f51e9b3dce32bec4a3997715ac=6525*1*10_19477688970-1568173861503-355956!5744*1*zl624867243; Hm_lvt_e5ef47b9f471504959267fd614d579cd=1572959537; Hm_ct_e5ef47b9f471504959267fd614d579cd=6525*1*10_19477688970-1568173861503-355956; __yadk_uid=pjj5GMdBequtcgJRO9FRFCBm2ujiBfE0; __gads=Test; UN=zl624867243; BT=1574056548132; acw_tc=2760822315752564645451539e97cbe7d9b073c224038154fadca2804746b0; TY_SESSION_ID=83852086-c7f3-4a6d-a54f-ea2747104dc5; dc_tos=q35i5o; c-login-auto=2; Hm_lpvt_6bcd52f51e9b3dce32bec4a3997715ac=1577416237; announcement=%257B%2522isLogin%2522%253Afalse%252C%2522announcementUrl%2522%253A%2522https%253A%252F%252Fblog.csdn.net%252Fblogdevteam%252Farticle%252Fdetails%252F103603408%2522%252C%2522announcementCount%2522%253A0%252C%2522announcementExpire%2522%253A3600000%257D; hasSub=true; acw_sc__v2=5e0576296c3844143dae65b0972cfe3fc0a8b303",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0"
}
# 获得文章列表urls
def getUrls(url):

    # 发送请求
    resp = requests.request("GET", url, data=payload, headers=headers)
    #设置解码方式
    resp.encoding=resp.apparent_encoding
    #这里会用设置的解码方式解码
    html_source = resp.text
    # 正则表达式，取出网页中的url链接（一些寻找注入点的工具也是这么做出来的）
    urls = re.findall("https://[^>\";\']*\d",html_source)
    new_urls=[]
    for url in urls:
        if 'details' in url:
            if url not in new_urls:
                new_urls.append(url)
    return new_urls

urls = getUrls("https://blog.csdn.net/knight_zhou")
# print(urls)
# print(len(urls))

# while True:
#     for url in urls:
#         requests.request("GET", url, data=payload, headers=headers)
#         print(url, "Ok")
#         time.sleep(5)
#     time.sleep(5)

## 测试访问
for url in urls:
    requests.request("GET",url,data=payload,headers=headers)
    print(urls,"ok")
