# coding:utf-8
import requests
import re
import time
import os
import webbrowser

## 设置变量
payload = ""

#设置打开时间
def killbrowser():
    cmd = "taskkill /F /IM SogouExplorer.exe"
    os.system(cmd)

# 请求头
headers = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0"
}
# 获得文章列表urls
def getUrls(url):

    # 发送请求
    resp = requests.request("GET", url, data=payload, headers=headers)
    #设置解码方式
    resp.encoding=resp.apparent_encoding
    #这里会用设置的解码方式解码
    html_source = resp.text
    # 正则表达式，取出网页中的url链接（一些寻找注入点的工具也是这么做出来的）
    urls = re.findall("https://[^>\";\']*\d",html_source)
    new_urls=[]
    for url in urls:
        if 'details' in url:
            if url not in new_urls:
                new_urls.append(url)
    return new_urls

urls = getUrls("https://blog.csdn.net/knight_zhou")
# print(urls)


## 循环打开页面
for url in urls:
    # print(url)
    time.sleep(5)
    webbrowser.open(url)

# 最后kill掉进程
killbrowser()