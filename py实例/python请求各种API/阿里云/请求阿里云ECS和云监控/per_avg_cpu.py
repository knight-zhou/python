#!/usr/bin/env python
#coding:utf-8
# cpu百分比
from aliyunsdkcore import client
from aliyunsdkcms.request.v20180308 import QueryMetricListRequest  #基础云监控数据采集无须安装插件
from aliyunsdkcms.request.v20180308 import QueryMetricLastRequest  #最新数据,云监控采集
import time,datetime
import json
from ecs_id import  ecs_id

clt = client.AcsClient('xx','oo','cn-shenzhen')
#request = QueryMetricListRequest.QueryMetricListRequest()
request = QueryMetricLastRequest.QueryMetricLastRequest()  #最新数据
request.set_accept_format('json')

request.set_Project('acs_ecs_dashboard')
request.set_Metric('CPUUtilization')
start_time=(datetime.datetime.now()-datetime.timedelta(minutes=2)).strftime("%Y-%m-%d %H:%M:%S")
# print start_time

timestamp_start = int(time.mktime(time.strptime(start_time, "%Y-%m-%d %H:%M:%S"))) * 1000
request.set_StartTime(timestamp_start)

#首先清空文件
f1 = open('ecs_avg_cpu.txt', 'a')
f1.seek(0)
f1.truncate()
f1.close()

#每个ecs的平均值写入文件
for ecs in ecs_id:
    # idid="i-wz9f44r0ae83vp8uvjei"
    yy="{'instanceId':'%s'}" %(ecs)
    request.set_Dimensions(yy)
    request.set_Period('60')
    result = clt.do_action_with_exception(request)
    aa = json.loads(result)
    cc = json.loads(aa['Datapoints'])
    avg_cpu= cc[0]['Average']
    str_avg_cpu=str(avg_cpu)

    f = open('ecs_avg_cpu.txt', 'a')
    f.write(str_avg_cpu)
    f.write("\n")

    time.sleep(0.2)

f.close()
print "写入文件完成............."    
