#!/bin/bash
avg_cpu=$(awk '{for(n=1;n<=NF;++n)a[n]+=$n}END{for(n=1;n<=NF;++n)$n=a[1]/NR;print}' ecs_avg_cpu.txt)
echo $avg_cpu |awk '{printf "%.2f",$0}'
