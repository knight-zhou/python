#!/bin/bash
avg_mem=$(awk '{for(n=1;n<=NF;++n)a[n]+=$n}END{for(n=1;n<=NF;++n)$n=a[1]/NR;print}' ecs_avg_mem.txt)
echo $avg_mem |awk '{printf "%.2f",$0}'
