#coding:utf-8
# 找 InstanceId 值
# PageNumber:实例状态列表的页码   PageSize:分页查询时设置的每页行数
from aliyunsdkcore import client
from aliyunsdkecs.request.v20140526 import DescribeInstancesRequest
import json

client = client.AcsClient('xx','oo','cn-shenzhen')

# 设置参数
request = DescribeInstancesRequest.DescribeInstancesRequest()
request.set_Status("Running")

ecs_id=[]
# ecs_id_x=[]

for line in range(1,4):
    request.set_PageSize(100) # 返回行数
    request.set_PageNumber(line) # 返回页码
    request.set_accept_format('json')
    response = client.do_action_with_exception(request)
    dictinfo = json.loads(response)
    bb = dictinfo["Instances"]['Instance']
    for i in range(len(bb)):
        yy=bb[i]['InstanceId']
        ecs_id.append(yy)
        
if __name__=="__main__":
    print len(ecs_id)
    

