#!/bin/bash
avg_disk_used=$(awk '{for(n=1;n<=NF;++n)a[n]+=$n}END{for(n=1;n<=NF;++n)$n=a[1]/NR;print}' ecs_avg_disk_used.txt)
echo $avg_disk_used |awk '{printf "%.2f",$0}'
