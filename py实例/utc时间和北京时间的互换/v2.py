# coding:utf-8
import datetime
utc="2019-08-24T14:57:08.304Z"
UTC_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
utcTime = datetime.datetime.strptime(utc, UTC_FORMAT)
localtime = utcTime + datetime.timedelta(hours=8)
print(localtime)
print(localtime.strftime('%Y-%m-%d %H:%M:%S'))