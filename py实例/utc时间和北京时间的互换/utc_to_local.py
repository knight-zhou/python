import datetime
utc="2019-10-29T20:45:06.631494166+08:00"
utc=utc.split('.')[0]
def utc2local(utc):
    UTC_FORMAT = "%Y-%m-%dT%H:%M:%S"
    utcTime = datetime.datetime.strptime(utc, UTC_FORMAT)
    l_time = utcTime + datetime.timedelta(hours=8)
    localtime=l_time.strftime('%Y-%m-%d %H:%M:%S')
    print(localtime)
    return localtime

utc2local(utc)
