#!/usr/bin/env python3
# @Author : knight
import os, json

path = "c:/test"
def path_convert(path):
   d = {}
   if os.path.isdir(path):
      d['text'] = os.path.basename(path)
      d['children'] = [ path_convert(os.path.join(path,x)) for x in sorted(os.listdir(path)) ]
   else:
      d['text'] = os.path.basename(path)
   return d


# res = path_convert(path)
# # print(res)
# print(json.dumps(res))

print(json.dumps([path_convert(y) for y in sorted(os.listdir('.'))]))