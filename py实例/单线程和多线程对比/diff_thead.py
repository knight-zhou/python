# coding:utf-8
from multiprocessing import Process
from threading import  Thread

def add_fun(num):
    for i in range(50000000):
        num+=i
    print(num)
    return num
#多线程加法
def thread_add(num_list):
    thread_list=[]
    for i in num_list:
        t=Thread(target=add_fun,args=[i])
        t.start()
        thread_list.append(t)
    for t in thread_list:
        t.join()

#单线程加法
def single_add(num_list):
    for i in num_list:
        add_fun(i)

if __name__ == '__main__':
    num_list = [1, 2, 3, 4]
    # single_add(num_list)
    thread_add(num_list)