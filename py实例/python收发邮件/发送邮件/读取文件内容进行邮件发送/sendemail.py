#!/usr/bin/python
#-*- coding: UTF-8 -*-
"""
发送邮件脚本，支持代理(使用-p参数打开代理开关)
author: andrew.liu 2017-06-23
Usage:
	python sendemail.py -t "xxx1@xx.com;xxx2@xx.com" -s "mail subject" -m "mail body"
"""

#代理信息配置
proxy_host='100.98.1.139'
proxy_port=3129
proxy_user='dbuser'
proxy_pass='justf0rhlldbhostOnline'
rdns_host='127.0.0.1'

#发件人信息配置
mail_host='smtp.exmail.qq.com'
mail_user='tech.sys@xxoo.com'
mail_pass='1213456'

import sys,os

#模块检查
try:
	import socks
except Exception:
	print "\033[1;33;1mTrying install module pysocks... \033[0m"
	try:
		CmdLine="rpm -q python2-pysocks || yum -y install python2-pysocks;" 
		os.system(CmdLine)
		import socks
		print "\033[1;32;5mInstall module pysocks successfully. \033[0m"
	except Exception as e:
		print e
		print "\033[1;31;5mInstall module pysocks failure. \033[0m"
		sys.exit(1)	
import re
import getopt
import smtplib
from email.mime.text import MIMEText 
from email.Header import Header

#帮助
def Usage():
	print """\033[1;33;1mError!\033[0m
	Usage: python %s -t <To mail> -s <Subject> -m <mail body>

	Parameters:
		-t\tAddressee, Between multiple use ';' split, Example: "a1@abc.com;a2@abc.com"
		-s\tMail subject
		-m\tMail body, Using HTML format, Example: "hello<br>world<br>"

		Optional:
			-p\tUsing proxy switch
	""" % sys.argv[0]
	sys.exit(1)

def send_mail(to_list,sub,content,use_proxy=False):
        try:
	        me="tech.sys<" + mail_user + ">"
    	        msg=MIMEText(content,_subtype='html',_charset='utf-8')
    	        msg['Subject']=Header(sub,"UTF-8")
	        msg['From']=me
	        msg['To']=to_list
                #判断多个收件人地址是用,还是;做分隔的
                listtemp=re.compile(r',',re.IGNORECASE)
                if listtemp.findall(to_list):
                    MailToList=to_list.split(',')
                else:
                    MailToList=to_list.split(';')
		#如果使用代理，则配置走socket代理
		if use_proxy == True:
			try:
				socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,proxy_host,proxy_port,rdns_host,proxy_user,proxy_pass)
				socks.wrapmodule(smtplib)
			except Exception as e:
				print e
				return False	
		smtp = smtplib.SMTP(mail_host,timeout=10)
		#smtp.connect(mail_host)
		smtp.login(mail_user, mail_pass)
		smtp.sendmail(me,MailToList, msg.as_string())
		smtp.close()
		return True
	except Exception,e:
		print str(e)
		return False

#参数个数检查
if len(sys.argv) < 7 or len(sys.argv) > 8:
	Usage()
#初始化参数
MailTo=''
Subject=''
MailBody=''
use_proxy=False
#输入参数处理
opts,args=getopt.getopt(sys.argv[1:],"t:s:m:p")
for op,value in opts:
	if op == "-t":
		MailTo=value.lstrip()
	elif op == "-s":
		Subject=value.lstrip()
	elif op == "-m":
		MailBody=value.lstrip().replace('\n','<br>')
	elif op == "-p":
		use_proxy=True
	else:
		Usage()

if send_mail(MailTo,Subject,MailBody,use_proxy):
	print "Send email \033[1;32;1msuccessfully\033[0m"
	sys.exit(0)
else:	
	print "Send email \033[1;31;1mfailure\033[0m"
	sys.exit(1)
