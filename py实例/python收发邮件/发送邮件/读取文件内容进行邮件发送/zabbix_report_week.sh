#!/bin/bash
#统计报表

mail_res=knight.zhou@qq.com

now=`date "+%Y-%m-%d %H:%m:%S"`
today=`date "+%Y-%m-%d"`
num=`cat alert.txt |grep -c "row"`

dir_res="/home/data/scripts/zabbix_about/report/reults_report"
dir_res_day="$dir_res/$today"
## 建立目录
mkdir -p $dir_res_day

## 首先清空文件
> $dir_res_day/report_all.txt
> $dir_res/one_week/report_all.txt



Report_cpu(){
	## 获取所有主机cpu idle time
	zabbix_api --report "CPU idle time" "$today 00:00:00" "$now" --table  >> $dir_res_day/report_all.txt
	# 写入excel
	zabbix_api --report "CPU idle time" "$today 00:00:00" "$now" --table --title "cpu idle time" --xls $dir_res_day/"$today"_cpu.xls

}

Report_free_disk(){
	##获取分区使用率
	echo "根分区使用情况:" >> $dir_res_day/report_all.txt
	zabbix_api --report "Free disk space on / (percentage)" "$today 00:00:00" "$now" --table  >> $dir_res_day/report_all.txt
	
	echo "/home/data 分区 使用情况:" >> $dir_res_day/report_all.txt
	zabbix_api --report "Free disk space on /home/data (percentage)" "$today 00:00:00" "$now" --table  >> $dir_res_day/report_all.txt

	# 写入excel
	zabbix_api --report "Free disk space on /home/data (percentage)" "$today 00:00:00" "$now" --table --title "/home/data 分区磁盘使用率" --xls $dir_res_day/"$today"_home_data_pfree.xls
	zabbix_api --report "Free disk space on / (percentage)" "$today 00:00:00" "$now" --table --title "根分区磁盘使用率" --xls $dir_res_day/"$today"_root_pfree.xls
	

}

Report_send(){
	#mysql -uselect_all -pselect_all -e "SELECT alertid,SUBJECT,message FROM zabbix.alerts WHERE sendto = 'knight.zhou@fang.cn' AND subject LIKE '故障%' AND DATE_FORMAT( FROM_UNIXTIME(clock),'%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')\G" > ./alert.txt

	mysql -uselect_all -pselect_all -e "SELECT alertid,SUBJECT,message FROM zabbix.alerts WHERE sendto = 'knight.zhou@fang.cn' AND subject LIKE '故障%' AND DATE_FORMAT( FROM_UNIXTIME(clock),'%Y-%m-%d') <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)\G" > ./alert.txt
	dos2unix ./alert.txt

}

Chuli_alert(){
	echo -e "     次数  主机\t\t\t\t   告警项目" > $dir_res_day/top_triger.txt
	#cat ./alert.txt|sed 's/message://g' |egrep -e "告警信息"  -e "告警主机" | sed 'N;s/\n/ :/' |awk -F ':' '{print $3"\t"$5} {print "总数"}'|sort |uniq -c|sort -r >> $dir_res_day/top_triger.txt
	cat ./alert.txt|sed 's/message://g' |egrep -e "告警信息"  -e "告警主机" | sed 'N;s/\n/ |/' |awk -F '[:|]' '{print $2"\t"$4}'|sort |uniq -c|sort -k1nr >> $dir_res_day/top_triger.txt
	echo "###"
	## 写入report_all
	cat $dir_res_day/top_triger.txt >> $dir_res_day/report_all.txt
	echo "上周告警统计:" >> $dir_res/one_week/report_all.txt
	cat $dir_res_day/top_triger.txt >> $dir_res/one_week/report_all.txt
	echo -e  "    总数:  $num" >> $dir_res/one_week/report_all.txt
}


Send_mail(){
	body="$dir_res/one_week/report_all.txt"
	#echo $body
	python /home/data/scripts/sendemail.py  -t $mail_res  -s “周报表统计” -m "`cat  ${body}`"
}

Report_send
Chuli_alert

Send_mail

#echo "##########################################################################################以下是CPU 使用情况 #########################################################" >> $dir_res_day/report_all.txt
#Report_cpu
#echo "##########################################################################################以下是磁盘使用率情况 #########################################################" >> $dir_res_day/report_all.txt
#Report_free_disk
#python  /home/data/scripts/sendemail.py  -t $Email  -s "$host ${j}.hll.cn  $i code count is $count" -m "`cat ${j}_${i}_codecontent.txt`"  -p
