# coding:utf-8
## python a.py '{"jname":"abc","numprocs":"14"}'

import json
import jinja2
import sys


## 定义变量
num = len(sys.argv)
#json_data=sys.argv[1]

file='./file.txt'
# 判断是否为json
def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError:
        return False
    return True
###
if num ==2 and is_json(sys.argv[1]):
    pass    # 执行下一步操作
else:
    print("请只传入一个参数并且是json数据,脚本退出")
    sys.exit()

context=json.loads(sys.argv[1])   # 转成字典

src_file = open(file).read()   # 读取文件为字符串
with open('result.txt','w') as f:
    result = jinja2.Template(source=src_file).render(**context)
    f.write(result)

