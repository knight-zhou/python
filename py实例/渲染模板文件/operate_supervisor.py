#! /usr/bin/env python
# -*- coding: utf-8 -*-
## python a.py '{"jname":"abc","numprocs":"14"}'
## pls pip install jianja2 first

import json
import jinja2
import sys,os

supervisor_config = {
    'dev':{
        'jname' : '', #服务名，对应lalaplat的业务名
        'java_home' : '/usr/local/jdk1.8.0_172/bin/java',
        'jvm_mem' : '512M', #jvm的内存使用，默认512M
        'env' : 'DEV', #阿拉伯服务环境，FAT[stg] UAT[gra] PRO[prd]，默认PRO
        'appenv' : 'dev', #服务器环境，stg gra prd，默认prd
        'envadd' : 'http://apollo-dev.k.cn:8080', #阿波罗地址
        'protocol' : 'http', #服务协议，默认http 【rpc】
        'numprocs' : 1, #启动的进程数，默认1个
        'jport': '',  # 启动端口号段（前缀），比如要启动2个端口，分别为1240，1241，则jport=124，剩下124后面的数字是由supervisor自己添加的，是根据numprocs的值来定的
        'app_dir' : '/home/data/webroot/', #服务所在的根路径,如/home/data/webroot/position-gw/
        'app_path' : '', #服务所在的地址，相对与根路径的，即jar包地址，如lib/report-gw.jar
        'user' : 'wwwuser', #启动的用户，默认wwwuser
        'app_log_path' : '/home/data/logs/' #日志路径
    },
    'stg':{
        'jname' : '', #服务名，对应lalaplat的业务名
        'java_home' : '/usr/local/jdk1.8.0_172/bin/java',
        'jvm_mem' : '512M', #jvm的内存使用，默认512M
        'env' : 'FAT', #阿拉伯服务环境，FAT[stg] UAT[gra] PRO[prd]，默认PRO
        'appenv' : 'stg', #服务器环境，stg gra prd，默认prd
        'envadd' : 'http://apollo-stg.k.cn:8080', #阿波罗地址
        'protocol' : 'http', #服务协议，默认http 【rpc】
        'numprocs' : 1, #启动的进程数，默认1个
        'jport': '',  # 启动端口号段（前缀），比如要启动2个端口，分别为1240，1241，则jport=124，剩下124后面的数字是由supervisor自己添加的，是根据numprocs的值来定的
        'app_dir' : '/home/data/webroot/', #服务所在的根路径,如/home/data/webroot/position-gw/
        'app_path' : '', #服务所在的地址，相对与根路径的，即jar包地址，如lib/report-gw.jar
        'user' : 'wwwuser', #启动的用户，默认wwwuser
        'app_log_path' : '/home/data/logs/' #日志路径
    },
    'gra':{
        'jname' : '', #服务名，对应lalaplat的业务名
        'java_home' : '/usr/local/jdk1.8.0_172/bin/java',
        'jvm_mem' : '512M', #jvm的内存使用，默认512M
        'env' : 'UAT', #阿拉伯服务环境，FAT[stg] UAT[gra] PRO[prd]，默认PRO
        'appenv' : 'gra', #服务器环境，stg gra prd，默认prd
        'envadd' : 'http://apollo-gra.k.cn:8080', #阿波罗地址
        'protocol' : 'http', #服务协议，默认http 【rpc】
        'numprocs' : 1, #启动的进程数，默认1个
        'jport': '',  # 启动端口号段（前缀），比如要启动2个端口，分别为1240，1241，则jport=124，剩下124后面的数字是由supervisor自己添加的，是根据numprocs的值来定的
        'app_dir' : '/home/data/webroot/', #服务所在的根路径,如/home/data/webroot/position-gw/
        'app_path' : '', #服务所在的地址，相对与根路径的，即jar包地址，如lib/report-gw.jar
        'user' : 'wwwuser', #启动的用户，默认wwwuser
        'app_log_path' : '/home/data/logs/' #日志路径
    },
    'prd':{
        'jname' : '', #服务名，对应lalaplat的业务名
        'java_home' : '/usr/local/jdk1.8.0_172/bin/java',
        'jvm_mem' : '512M', #jvm的内存使用，默认512M
        'env' : 'PRO', #阿拉伯服务环境，FAT[stg] UAT[gra] PRO[prd]，默认PRO
        'appenv' : 'prd', #服务器环境，stg gra prd，默认prd
        'envadd' : 'http://apollo.k.cn:8080', #阿波罗地址
        'protocol' : 'http', #服务协议，默认http 【rpc】
        'numprocs' : 1, #启动的进程数，默认1个
        'jport': '',  # 启动端口号段（前缀），比如要启动2个端口，分别为1240，1241，则jport=124，剩下124后面的数字是由supervisor自己添加的，是根据numprocs的值来定的
        'app_dir' : '/home/data/webroot', #服务所在的根路径,如/home/data/webroot/position-gw/
        'app_path' : '', #服务所在的地址，相对与根路径的，即jar包地址，如lib/report-gw.jar
        'user' : 'wwwuser', #启动的用户，默认wwwuser
        'app_log_path' : '/home/data/logs' #日志路径
    }
}


config_path = "/etc/supervisord.d/"
config_module_file = "/home/data/webroot/lalaplat_issuer/scripts/supervisor_default.conf"
# config_module_file = "supervisor_default.conf"
num = len(sys.argv)

def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError:
        return False
    return True

def delete_file(conf_file):
    is_del = False
    if os.path.exists(conf_file):
        try:
            os.remove(conf_file)
            is_del = True
        except OSError:
            is_del = False
            return is_del

    return is_del

###
if num >= 2 and is_json(sys.argv[1]):
    pass
else:
    #print("请只传入一个参数并且是json数据")
    ret = {'ret': -1, 'data': {}, 'msg': '参数错误'}
    print json.dumps(ret, ensure_ascii=False)
    print "FAIL"
    sys.exit(0)

json_value = json.loads(sys.argv[1])
if 'jname' not in json_value or json_value['jname'] is '' \
        or 'appenv' not in json_value or json_value['appenv'] not in ['stg', 'gra', 'prd'] \
        or 'jport' not in json_value or json_value['jport'] is '' \
        or 'app_path' not in json_value or json_value['app_path'] is '':
    ret = {'ret': -1, 'data': {}, 'msg': '必填参数不能为空'}
    print json.dumps(ret, ensure_ascii=False)
    print "FAIL"
    sys.exit(3)

j_name = json_value['jname']
config_name = j_name + ".conf"
config_name = os.path.join(config_path, config_name)
if sys.argv[2] is not None and sys.argv[2] and 'delete' == sys.argv[2]:
    is_del = delete_file(config_name)
    if is_del == True:
        print "SUCC"
        sys.exit(0)
    else:
        print "FAIL"
        sys.exit(3)
else:
    sys_env = json_value['appenv']
    context = supervisor_config[sys_env]
    context['jname'] = j_name
    if 'jvm_mem' in json_value and json_value['jvm_mem']:
        context['jvm_mem'] = json_value['jvm_mem']
    if 'numprocs' in json_value and json_value['numprocs']:
        context['numprocs'] = json_value['numprocs']
    if 'jport' in json_value and json_value['jport']:
        context['jport'] = json_value['jport']
    if 'protocol' in json_value and json_value['protocol'] and json_value['protocol'].lower() in ['http', 'rpc']:
        context['protocol'] = json_value['protocol'].lower()
    if 'app_path' in json_value and json_value['app_path']:
        context['app_path'] = json_value['app_path']

    context['app_dir'] = os.path.join(context['app_dir'], j_name, '')
    context['app_log_path'] = os.path.join(context['app_log_path'], j_name, '')

    src_file = open(config_module_file).read()
    with open(config_name, 'w') as f:
        result = jinja2.Template(source=src_file).render(**context)
        f.write(result)

    ret = {'ret': 0, 'data': {}, 'msg': 'OK'}
    print json.dumps(ret, ensure_ascii=False)
    print "SUCC"
    sys.exit(0)

#python operate_supervisor.py '{"jname":"position-report-gw","jvm_mem":"1G","protocol":"http","jport":"700","app_path":"lib/position-report-gw.jar","numprocs":"1","appenv":"stg"}'
#python operate_supervisor.py '{"jname":"position-report-gw","jvm_mem":"1G","protocol":"http","jport":"700","app_path":"lib/position-report-gw.jar","numprocs":"1","appenv":"stg"}' 'delete'
