# coding:utf-8
# py2
import redis
  
redis_server = redis.StrictRedis(host='47.93.89.146', port=6389)
  
# 建立PubSub对象订阅通道和侦听新消息
p = redis_server.pubsub()
  
# 订阅两个频道，分别是my-first-channel，和my-second-channel
p.subscribe('channel', 'my-second-channel')
  
# 使用p.listen()方法监听频道
for item in p.listen():  
    # 如果订阅的频道存储的项目是'message'
    if item['type'] == 'message':  
        # 则打印这个项目
        print item['data']