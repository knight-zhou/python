# coding: utf-8
# #py2
import redis
  
redis_client = redis.StrictRedis(host='47.93.89.146', port=6389)
  
# 建立PubSub对象订阅通道和侦听新消息
p = redis_client.pubsub()
  
# 订阅两个频道，分别是my-first-channel，和my-second-channel
p.subscribe('channel', 'my-second-channel')
  
# 使用publish推送消息,注意是StrictRedis类的方法
redis_client.publish('channel', '6666')