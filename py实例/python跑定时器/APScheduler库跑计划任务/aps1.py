#!/usr/bin/env python
#coding:utf-8
import datetime
from apscheduler.schedulers.blocking import BlockingScheduler

scheduler = BlockingScheduler()
def test():
    print "now is {0}".format(datetime.datetime.now())

scheduler.add_job(test,"cron",second="*/3")  #调用add_job方法

try:
    scheduler.start()
except(KeyboardInterrupt,SystemExit):
    scheduler.shutdown()



