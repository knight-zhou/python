#!/usr/bin/env python
#coding:utf-8
# 使用装饰器代替实现

import datetime  
from apscheduler.schedulers.blocking import BlockingScheduler  
  
scheduler = BlockingScheduler()  
 
 
@scheduler.scheduled_job("cron", second="*/3")  
def test():  
    print "now is:{0}".format(datetime.datetime.now())  
  
try:  
    scheduler.start()  
except (KeyboardInterrupt, SystemExit):  
    scheduler.shutdown() 