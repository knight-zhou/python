
[极客学院搭建博客](http://wiki.jikexueyuan.com/project/django-set-up-blog)

[官方中文文档_py3.7](https://docs.python.org/zh-cn/3.7/installing/index.html)

### windows同一台电脑如何切换成py2 or py3
```
py2:
C:\Users\Administrator>py -2

py3:
C:\Users\Administrator>py -3

```

python 添加新模块的方法：
```
# coding:utf-8
import sys
# print (sys.path)   # 查看模块路径
sys.path.append("/added_module")
print sys.path
```
##### py为什么有全局锁
* 全局锁，同一时刻只准许一个线程在跑，这样的目的是为了防止多个线程访问同一资源排斥的问题，所有就有互斥锁的概念
* php 天生不支持线程，官方也不建议
* 全局锁是解释性语言的通病，所以解释性语言执行效率比编译型语言效率低

python多线程也只能利用单核但是确实是在并发。

在cpu密集型的时候 单线程有时候比多线程快？是因为有全局锁的影响，因为线程切换需要时间所以有时候慢

那为什么在IO密集型的时候，多线程一定要快了，因为单线程操作IO的时候都需要IO等待多线程虽然只能利用单核，但是在IO等待的时候

线程切换了去做其他io操作了 所以看上去 多线程在IO操作方面比单线程要快

多进程可以利用多核，因为每个进程是一个独立的全局锁，但是这样进程之间通信就困难了。

一般是在主线程上使用queue 来实现通信 这个就和php差不多了


###
字符串的encode 是讲字符串编码成byte类型,decode则相反

```
aa = "hello"
print(aa.encode())
```

##### Python发送post的json请求

```
import requests
import json
url = "https://mp-dev.xx.cn/lala-mall/api/users/getUserInfo"

params = {"clueFromCode":"","phone":18877210282,"source":"H5"}

headers = {"Content-type": "application/json","Accept": "*/*"}

r = requests.post(url=url,data=json.dumps(params),headers=headers)
print(r.text)

```

