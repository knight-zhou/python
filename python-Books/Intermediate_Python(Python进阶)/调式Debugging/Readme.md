## pdb命令列表：

continue 或 c: 继续执行

w: 显示当前正在执行的代码行的上下文信息

a: 打印当前函数的参数列表

step 或 s: 执行当前代码行，并停在第一个能停的地方（相当于单步进入）

next 或 n: 继续执行到当前函数的下一行，或者当前行直接返回（单步跳过）

list 或 l:  查看当前行的代码段

exit 或 q:  中止并退出

单步跳过（next）和单步进入（step）的区别在于， 单步进入会进入当前行调用的函数内部并停在里面,而单步跳过会（几乎）全速执行完当前行调用的函数，并停在当前函数的下一行。

pdb真的是一个很方便的功能，上面仅列举少量用法，更多的命令强烈推荐你去看如下官方文档:

[py2-pdb](https://docs.python.org/2/library/pdb.html) 

[py3-pdb](https://docs.python.org/3/library/pdb.html)