#coding:utf-8
#需求: 你希望在脚本和程序中将诊断信息写入日志文件。
import logging
import os
logging.basicConfig(filename='app.log',level=logging.ERROR)  # 写在外面引入的时候可以找到并打到指定的文件
# g_logger = logging.getLogger(__name__)

def main():
    # Configure the logging system
    # logging.basicConfig(filename='app.log',level=logging.ERROR)
    # logging.basicConfig(filename='app.log',level=logging.INFO)  #如果定义在函数里面(app.log),执行程序必须在函数里面才能捕获，原因：作用域问题
    
    # 输出到文件,设置日志级别，只对error日志输出到文件
    
    # logging.basicConfig(level=logging.INFO)   #不接参数，输出到标准输出


    logging.critical('critical log:')
    logging.error("error log:")
    logging.warning('warning log:')
    logging.info('info log:')
    logging.debug('debug log:')

try:
	os.chdir("g:")
except FileNotFoundError as e:
	logging.error(str(e))        #捕获异常并转成str 写入日志


if __name__ == '__main__':
    main()