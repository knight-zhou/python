#coding:utf-8
#需求: 你希望在脚本和程序中将诊断信息写入日志文件。
import logging
logging.basicConfig(filename='app.log',level=logging.ERROR)  # 写在外面引入的时候可以找到并打到指定的文件
g_logger = logging.getLogger(__name__)

def main():
    logging.critical('critical log:')
    logging.error("error log:")
    logging.warning('warning log:')
    logging.info('info log:')
    logging.debug('debug log:')


if __name__ == '__main__':
    main()