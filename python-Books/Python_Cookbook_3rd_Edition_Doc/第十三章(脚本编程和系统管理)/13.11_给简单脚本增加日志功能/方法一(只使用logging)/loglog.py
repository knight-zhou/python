#coding:utf-8
#需求: 你希望在脚本和程序中将诊断信息写入日志文件。
import logging
import os
logging.basicConfig(filename='app.log',level=logging.ERROR)  # 写在外面引入的时候可以找到并打到指定的文件
# g_logger = logging.getLogger(__name__)

def main():
    # Configure the logging system
    # logging.basicConfig(filename='app.log',level=logging.ERROR)
    # logging.basicConfig(filename='app.log',level=logging.INFO)
    
       # 输出到文件,设置日志级别，只对error日志输出到文件
    
    # logging.basicConfig(level=logging.INFO)   #不接参数，输出到标准输出

    # Variables (to make the calls that follow work)
    hostname = 'www.python.org'
    item = 'knight'
    filename = 'data.csv'
    mode = 'r'

    # Example logging calls (insert into your program)
    logging.critical('Host %s unknown', hostname)
    logging.error("Couldn't find %r", item)
    logging.warning('Feature is deprecated')
    logging.info('Opening file %r, mode=%r', filename, mode)
    logging.debug('Got here')

try:
	os.chdir("g:")
except FileNotFoundError as e:
	logging.error(str(e))



# try:
# 	os.chdir("g:")
# except FileNotFoundError as e:
# 	logging.error(str(e))





if __name__ == '__main__':
    main()