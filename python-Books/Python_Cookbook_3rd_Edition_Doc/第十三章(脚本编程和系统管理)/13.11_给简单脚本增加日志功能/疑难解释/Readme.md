执行本目录的loglog.py 发下无法进行日志文件写入只能在标准输入输出：
原因是"作用域"的问题，因为main函数里定义的:
logging.basicConfig(filename='app.log',level=logging.ERROR) 

在函数外面是无法找到的，如果要打印出日志 可以将 try...expext...写到函数里面，或者将"app.log" 定义成全局的，
具体实验测试请看本目录下的test目录.
