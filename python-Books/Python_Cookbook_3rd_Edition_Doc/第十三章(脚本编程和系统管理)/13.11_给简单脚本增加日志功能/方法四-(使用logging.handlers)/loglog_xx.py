#coding:utf-8

import logging  
import logging.handlers 
import os 
  
LOG_FILE = 'tst.log'  
  
handler = logging.handlers.RotatingFileHandler(LOG_FILE, maxBytes = 1024*1024, backupCount = 5) # 实例化handler   
fmt = '%(asctime)s - %(filename)s:%(lineno)s - %(name)s - %(message)s'  
  
formatter = logging.Formatter(fmt)   # 实例化formatter  
handler.setFormatter(formatter)      # 为handler添加formatter  
  
logger = logging.getLogger('tst')    # 获取名为tst的logger  
logger.addHandler(handler)           # 为logger添加handler  
logger.setLevel(logging.DEBUG)       #设置日志的打印级别     
  
# logger.info('first info message')  
# logger.error('first error message')  
# logger.debug('first debug message') 
try:
	os.chdir('k:')
except FileNotFoundError as e:
	# logger.info(str(e))
	logger.error(str(e))