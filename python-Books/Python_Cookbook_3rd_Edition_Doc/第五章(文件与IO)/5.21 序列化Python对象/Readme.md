# 问题
你需要将一个Python对象(类是个对象，字符串也是一个对象，python中一切皆对象)序列化为一个字节流,以便将它保存到一个文件,存储到数据库或者通过网络传输它.
#### 将一个对象转储为字节流:
```
import pickle
data = "aa bb cc dd"
f = open('somefile','wb')
pickle.dump(data,f)  

```
#### 从字节流中恢复一个对象:
```
f1 = open('somefile','rb')
d1  = pickle.load(f1)
print (d1)
```

#### 如果将多个对象序列化存储为字节流了?
```
import pickle
f = open('one','wb')
pickle.dump([1,2,2],f)
pickle.dump('string data',f)
pickle.dump({'name':'knight','city':'sz'},f)

```
#### 从已经包含多个对象的字节流中恢复对象:
```
import pickle
f = open('one','rb')
print (pickle.load(f))
print (pickle.load(f))
print (pickle.load(f))

```


