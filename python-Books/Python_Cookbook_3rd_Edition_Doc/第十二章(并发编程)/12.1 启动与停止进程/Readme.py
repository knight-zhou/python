#coding:utf-8
import time
from threading import Thread

def count(n):
	while n > 0:
		print ('T-minus',n)
		n -= 1
		time.sleep(2)

# t = Thread(target=count,args=(5,))  # 前台线程
t = Thread(target=count,args=(5,),daemon=True)     #后台线程
t.start()
'''
当你创建好一个线程对象后，该对象并不会立即执行,除非你调用它的start()方法(当你调用start()方法时,它会调用你传递进来的函数，并把你传递进来的参数传递给该函数)
'''
# 查看线程状态
'''
if t.is_alive():
	print ('Still running')
else:
	print ('down')
'''
t.join()  # 将一个线程加入到当前线程,并等待它终止

"""
由于全局解释锁（GIL）的原因，Python 的线程被限制到同一时刻只允许一个线程执行这样一个执行模型。
所以，Python 的线程更适用于处理I/O和其他需要并发执行的阻塞操作（比如等待I/O、等待从数据库获取数据等等,而不是需要多处理器并行的计算密集型任务。
"""