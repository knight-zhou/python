当你想要构建一个将来需要序列化或编码成其他格式的映射的时候，`OrderedDict` 是非常有用的。 比如，你想精确控制以JSON编码后字段的顺序，你可以先使用 `OrderedDict` 来构建这样的数据：
```
>>>from collections import OrderedDict
>>>d = OrderedDict()
>>>d['foo'] = 1
>>>d['bar'] = 2
>>>d['spam'] = 3
>>>d['grok'] = 4
>>> import json
>>> json.dumps(d)
'{"foo": 1, "bar": 2, "spam": 3, "grok": 4}'
>>>
```
## 讨论
`OrderedDict`内部维护着一个根据键插入顺序排序的双向链表。每次当一个新的元素插入进来的时候,它会被放到链表的尾部,
(排列顺序按你之前插入的顺序进行排序)对于一个已经存在的键的重复赋值不会改变键的顺序。

