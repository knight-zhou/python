## 9.3. A First Look at Classes
Classes introduce a little bit of new syntax, three new object types, and some new semantics.

类引入了少量的新语法、三种新对象类型和一些新语义.

## 9.3.1. Class Definition Syntax

The simplest form of class definition looks like this(类定义的最简单形式如下所示):

```
class ClassName:
    <statement-1>
    .
    .
    <statement-N>
```
Class definitions, like function definitions (def statements) must be executed before they have any effect. (You could conceivably place a class definition in a branch of an if statement, or inside a function.)


In practice, the statements inside a class definition will usually be function definitions, but other statements are allowed, and sometimes useful — we’ll come back to this later. The function definitions inside a class normally have a peculiar form of argument list, dictated by the calling conventions for methods — again, this is explained later.

When a class definition is entered, a new namespace is created, and used as the local scope — thus, all assignments to local variables go into this new namespace. In particular, function definitions bind the name of the new function here.

When a class definition is left normally (via the end), a class object is created. This is basically a wrapper around the contents of the namespace created by the class definition; we’ll learn more about class objects in the next section. The original local scope (the one in effect just before the class definition was entered) is reinstated, and the class object is bound here to the class name given in the class definition header (ClassName in the example).

## 9.3.2. Class Objects(类对象)
Class objects support two kinds of operations: attribute references and instantiation.

类对象支持两种操作：属性引用和实例化。

Attribute references(属性引用): use the standard syntax used for all `attribute references` in Python: 
obj.name. Valid attribute names are all the names that were in the class’s namespace when the class object was created. 

有效的属性名称是在该类的命名空间中的类对象被创建时的所有名称
So, if the class definition looked like this:

```
class MyClass:
    """A simple example class"""
    i = 12345
    def f(self):
        return 'hello world'
```

then MyClass.i and MyClass.f are valid attribute references, returning an integer and a function object, respectively(分别). 

那么 MyClass.i 和 MyClass.f 是有效的属性引用，分别返回一个整数和一个函数对象.

Class attributes(属性) can also be assigned to(赋值), so you can change the value of MyClass.i by assignment(你可以通过给 MyClass.i 赋值来修改它).
 `__doc__` is also a valid(有效) attribute, returning the docstring(文档字符串) belonging to the class: "A simple example class".

Class instantiation(类的实例化) uses function notation(函数符号). Just pretend(假设) that the class object is a parameterless function(不带参数的函数) that returns a new instance of the class(返回一个新的类实例). For example (assuming the above class): 假设沿用上面的类

x = MyClass()

creates a new instance of the class and assigns this object to the local variable x.(创建这个类的一个新实例，并将该对象赋给局部变量x)

The instantiation operation (“calling” a class object) creates an empty object. Many classes like to create objects with instances customized to a specific initial state. Therefore a class may define a special method named `__init__()`, like this:

实例化操作（“调用”一个类对象）将创建一个空对象。很多类希望创建的对象可以自定义一个初始状态。因此类可以定义一个名为`__init__()`的特殊方法，像下面这样

```
def __init__(self):
    self.data = []
```
When a class defines an __init__() method, class instantiation automatically invokes __init__() for the newly-created class instance. So in this example, a new, initialized instance can be obtained by:

当类定义了`__init__()`方法，类的实例化会为新创建的类实例自动调用`__init__()`。所以在下面的示例中，可以获得一个新的、已初始化的实例

```
x = MyClass()
```

Of course, the `__init__()` method may have arguments for greater flexibility(可以带参数，更加灵活). 
In that case, arguments given to the class instantiation operator are passed on to `__init__()`. For example,

在这种情况下，类实例化操作的参数将传递给`__init__()`。
```
>>> class Complex:
...     def __init__(self, realpart, imagpart):
...         self.r = realpart
...         self.i = imagpart
...
>>> x = Complex(3.0, -4.5)
>>> x.r, x.i
(3.0, -4.5)
```
## 9.3.3. Instance Objects(实例对象)

Now what can we do with instance objects? (现在我们可以用实例对象做什么)
The only operations understood by instance objects are attribute references. There are two kinds of valid attribute names, data attributes and methods.

实例对象唯一可用的操作就是属性引用。有两种有效的属性名：数据属性和方法

data attributes correspond to “instance variables” in Smalltalk, and to “data members” in C++. Data attributes need not be declared; like local variables, they spring into existence when they are first assigned to. For example, if x is the instance of MyClass created above, the following piece of code will print the value 16, without leaving a trace:

数据属性相当于 Smalltalk 中的"实例变量"或C++中的"数据成员"。
数据属性不需要声明；和局部变量一样，它们会在第一次给它们赋值时生成。
例如，如果x是上面创建的MyClass的实例，下面的代码段将打印出值16而不会出现错误：

```
x.counter = 1
while x.counter < 10:
    x.counter = x.counter * 2
print x.counter
del x.counter
```

The other kind of instance attribute reference is a method. A method is a function that “belongs to” an object. (In Python, the term method is not unique to class instances: other object types can have methods as well. For example, list objects have methods called append, insert, remove, sort, and so on. However, in the following discussion, we’ll use the term method exclusively to mean methods of class instance objects, unless explicitly stated otherwise.)

实例属性引用的另一种类型是方法。方法是"属于"一个对象的函数.(在 Python 中，方法这个术语不只针对类实例：其他对象类型也可以具有方法。例如，列表对象有 append、insert、remove、sort 方法等等。但是在后面的讨论中，除非明确说明，我们提到的方法特指类实例对象的方法.)

Valid method names of an instance object depend on its class. By definition, all attributes of a class that are function objects define corresponding methods of its instances. So in our example, x.f is a valid method reference, since MyClass.f is a function, but x.i is not, since MyClass.i is not. But x.f is not the same thing as MyClass.f — it is a method object, not a function object.

实例对象的方法的有效名称依赖于它的类。根据定义，类中所有函数对象的属性定义了其实例中相应的方法。所以在我们的示例中， x.f是一个有效的方法的引用，因为MyClass.f是一个函数，但x.i不是，因为MyClass.i不是一个函数。但x.f与MyClass.f也不是一回事 —— 前者是一个方法对象，不是一个函数对象。

## 9.3.4. Method Objects
Usually, a method is called right after it is bound:(方法在绑定之后被直接调用)
```
x.f()
```
In the MyClass example, this will return the string 'hello world'. However, it is not necessary to call a method right away: x.f is a method object, and can be stored away and called at a later time. For example:
在MyClass的示例中，这将返回字符串'hello world'。然而，也不是一定要直接调用方法： x.f是一个方法对象，可以存储起来以后调用

```
xf = x.f
while True:
    print xf()
```
will continue to print hello world until the end of time.(会不断地打印hello world)

What exactly happens when a method is called? You may have noticed that x.f() was called without an argument above, even though the function definition for f() specified an argument. What happened to the argument? Surely Python raises an exception when a function that requires an argument is called without any — even if the argument isn’t actually used...

调用方法时到底发生了什么？你可能已经注意到，上面x.f()的调用没有参数，即使f ()函数的定义指定了一个参数。该参数发生了什么问题？当然如果函数调用中缺少参数 Python 会抛出异常——即使这个参数实际上没有使用……

Actually, you may have guessed the answer: the special thing about methods is that the object is passed as the first argument of the function. In our example, the call x.f() is exactly equivalent to MyClass.f(x). In general, calling a method with a list of n arguments is equivalent to calling the corresponding function with an argument list that is created by inserting the method’s object before the first argument.

实际上，你可能已经猜到了答案：方法的特别之处在于实例对象被作为函数的第一个参数传给了函数。在我们的示例中，调用x.f()完全等同于MyClass.f(x)。一般情况下，以n 个参数的列表调用一个方法就相当于将方法所属的对象插入到列表的第一个参数的前面，然后以新的列表调用相应的函数。

If you still don’t understand how methods work, a look at the implementation can perhaps clarify matters. When an instance attribute is referenced that isn’t a data attribute, its class is searched. If the name denotes a valid class attribute that is a function object, a method object is created by packing (pointers to) the instance object and the function object just found together in an abstract object: this is the method object. When the method object is called with an argument list, a new argument list is constructed from the instance object and the argument list, and the function object is called with this new argument list.

如果你还是不明白方法的工作原理，了解一下它的实现或许有帮助。引用非数据属性的实例属性时，会搜索它的类。如果这个命名确认为一个有效的类属性即函数对象，就会将实例对象和函数对象封装进一个抽象对象：这就是方法对象。以一个参数列表调用方法对象时，它被重新拆封，用实例对象和原始的参数列表构造一个新的参数列表，然后以这个新的参数列表调用对应的函数对象。

## 9.3.5. Class and Instance Variables(类和实例变量)
Generally speaking, instance variables are for data unique to each instance and class variables are for attributes and methods shared by all instances of the class:

一般来说，实例变量用于对每一个实例都是唯一的数据，类变量用于类的所有实例共享的属性和方法：
```
class Dog:

    kind = 'canine'         # class variable shared by all instances

    def __init__(self, name):
        self.name = name    # instance variable unique to each instance

>>> d = Dog('Fido')
>>> e = Dog('Buddy')
>>> d.kind                  # shared by all dogs
'canine'
>>> e.kind                  # shared by all dogs
'canine'
>>> d.name                  # unique to d
'Fido'
>>> e.name                  # unique to e
'Buddy'
```
As discussed in A Word About Names and Objects, shared data can have possibly surprising effects with involving mutable objects such as lists and dictionaries. For example, the tricks list in the following code should not be used as a class variable because just a single list would be shared by all Dog instances:

正如在名称和对象讨论的，可变对象例如列表和字典，共享数据可能带来意外的效果。例如，下面代码中的tricks 列表不应该用作类变量，因为所有的Dog 实例将共享同一个列表
```
class Dog:

    tricks = []             # mistaken use of a class variable

    def __init__(self, name):
        self.name = name

    def add_trick(self, trick):
        self.tricks.append(trick)

>>> d = Dog('Fido')
>>> e = Dog('Buddy')
>>> d.add_trick('roll over')
>>> e.add_trick('play dead')
>>> d.tricks                # unexpectedly shared by all dogs
['roll over', 'play dead']
```
Correct design of the class should use an instance variable instead:(这个类的正确设计应该使用一个实例变量)
```
class Dog:

    def __init__(self, name):
        self.name = name
        self.tricks = []    # creates a new empty list for each dog

    def add_trick(self, trick):
        self.tricks.append(trick)

>>> d = Dog('Fido')
>>> e = Dog('Buddy')
>>> d.add_trick('roll over')
>>> e.add_trick('play dead')
>>> d.tricks
['roll over']
>>> e.tricks
['play dead']
```

============================================================================

补充:

```
#!/usr/bin/python
# Filename: class_init.py

class Person:
    def __init__(self, name):     # 初始化函数,在c# 中称之为构造函数
        self.name = name
    def sayHi(self):
        print 'Hello, my name is', self.name

p = Person('Knight')
p.sayHi()
```
运行结果:
```
$ python class_init.py
Hello, my name is Knight
```
这里，我们把`__init__`方法定义为取一个参数name（以及普通的参数self）。在这个`__init__` 里,我们只是创建一个新的域，也称为name,注意他们是两个不同的变量，尽管他们有
相同的名字，点号使我们能够区分它们。

最重要的是，我们没有专门调用`__init__`方法，只是在创建一个类的新实例的时候(对象实例化的时候)，把参数包括在圆括号内跟在类名后面，从而传递给`__init__`方法。这是这种方法的重要之处。

现在，我们能够在我们的方法中使用`self.name`域。这在`sayHi`方法中得到了验证。










