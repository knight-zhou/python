## 9.5. Inheritance(继承)
Of course, a language feature would not be worthy of the name “class” without supporting inheritance. The syntax for a derived class definition looks like this:

当然，号称是“类”，却不支持继承，是配不上“类”这个名字的。派生类定义的语法如下所示：
```
class DerivedClassName(BaseClassName):
    <statement-1>
    .
    .
    .
    <statement-N>
```
The name BaseClassName must be defined in a scope containing the derived class definition. In place of a base class name, other arbitrary expressions are also allowed. This can be useful, for example, when the base class is defined in another module:

BaseClassName必须与派生类定义在一个作用域内。用其他任意表达式代替基类的名称也是允许的。这会很有用，例如，当基类定义在另一个模块中时：

```
class DerivedClassName(modname.BaseClassName):
```
Execution of a derived class definition proceeds the same as for a base class. When the class object is constructed, the base class is remembered. This is used for resolving attribute references: if a requested attribute is not found in the class, the search proceeds to look in the base class. This rule is applied recursively if the base class itself is derived from some other class.

There’s nothing special about instantiation of derived classes: DerivedClassName() creates a new instance of the class. Method references are resolved as follows: the corresponding class attribute is searched, descending down the chain of base classes if necessary, and the method reference is valid if this yields a function object.

Derived classes may override methods of their base classes. Because methods have no special privileges when calling other methods of the same object, a method of a base class that calls another method defined in the same base class may end up calling a method of a derived class that overrides it. (For C++ programmers: all methods in Python are effectively virtual.)

An overriding method in a derived class may in fact want to extend rather than simply replace the base class method of the same name. There is a simple way to call the base class method directly: just call BaseClassName.methodname(self, arguments). This is occasionally useful to clients as well. (Note that this only works if the base class is accessible as BaseClassName in the global scope.)

派生类定义的执行过程和基类是相同的。类对象创建后，基类会被保存。这用于解析属性的引用：如果在类中找不到请求的属性，搜索会在基类中继续。如果基类本身是由别的类派生而来，这个规则会递归应用。

派生类的实例化没有什么特殊之处：DerivedClassName()创建类的一个新的实例。方法的引用按如下规则解析： 搜索对应的类的属性，必要时沿基类链逐级搜索，如果找到了函数对象这个方法引用就是合法的。

派生的类能够重写其基类的方法。因为方法调用本对象中的其它方法时没有特权，基类的方法调用本基类的方法时，可能实际上最终调用了派生类中的覆盖方法。（对于 C++ 程序员：Python 中的所有方法实际上都是虚的。)

子类中的覆盖方法可能是想要扩充而不是简单的替代基类中的重名方法。有一个简单的方法可以直接调用基类方法：只要调用BaseClassName.methodname(self, arguments)。有时这对于客户端也很有用。（要注意只有BaseClassName在同一全局作用域定义或导入时才能这样用。）

Python has two built-in functions that work with inheritance:(Python 有两个与继承相关的内置函数)

* Use isinstance() to check an instance’s type: isinstance(obj, int) will be True only if `obj.__class__ `is int or some class derived from int. 
使用isinstance()来检查实例类型：isinstance(obj, int)只有`obj.__class__`是int或者是从int派生的类时才为True。

* Use issubclass() to check class inheritance: issubclass(bool, int) is True since bool is a subclass of int. However, issubclass(unicode, str) is False since unicode is not a subclass of str (they only share a common ancestor, basestring). 

使用issubclass()来检查类的继承: issubclass(bool， int)是True因为bool是int的子类。然而，issubclass(unicode,str)是False因为unicode不是str的一个子类（它们只是共享一个祖先， basestring).

## 9.5.1. Multiple Inheritance(多继承) -- 使用较少
Python supports a limited form of multiple inheritance as well. A class definition with multiple base classes looks like this:

Python 也支持一定限度的多继承形式。具有多个基类的类定义如下所示
```
class DerivedClassName(Base1, Base2, Base3):
    <statement-1>
    .
    .
    .
    <statement-N>

```
For old-style classes, the only rule is depth-first, left-to-right. Thus, if an attribute is not found in DerivedClassName, it is searched in Base1,then(recursively) in the base classes of Base1, and only if it is not found there, it is searched in Base2, and so on.

对于经典类，唯一的规则是深度优先，从左到右。因此，如果在DerivedClassName中找不到属性，它搜索Base1，然后（递归）基类中的Base1，只有没有找到，它才会搜索base2，依此类推

(To some people breadth first — searching Base2 and Base3 before the base classes of Base1 — looks more natural. However, this would require you to know whether a particular attribute of Base1 is actually defined in Base1 or in one of its base classes before you can figure out the consequences of a name conflict with an attribute of Base2. The depth-first rule makes no differences between direct and inherited attributes of Base1.)

（对某些人，广度优先——在搜索Base1的基类之前先搜索base2和Base3——看起来更自然。然而，在你能弄明白Base1的某个特定属性与base2中的一个属性名称冲突的后果之前，你需要知道该特定属性实际上是定义在Base1中还是在其某个基类中。深度优先规则使Base1的直接属性和继承的属性之间没有差别）。

For new-style classes, the method resolution order changes dynamically to support cooperative calls to super(). This approach is known in some other multiple-inheritance languages as call-next-method and is more powerful than the super call found in single-inheritance languages.

对于新式类，方法的解析顺序动态变化地支持合作地对super()进行调用。这种方法在某些其它多继承的语言中也有并叫做call-next-method，它比单继承语言中的super调用更强大。

With new-style classes, dynamic ordering is necessary because all cases of multiple inheritance exhibit one or more diamond relationships (where at least one of the parent classes can be accessed through multiple paths from the bottommost class). For example, all new-style classes inherit from object, so any case of multiple inheritance provides more than one path to reach object. To keep the base classes from being accessed more than once, the dynamic algorithm linearizes the search order in a way that preserves the left-to-right ordering specified in each class, that calls each parent only once, and that is monotonic (meaning that a class can be subclassed without affecting the precedence order of its parents). Taken together, these properties make it possible to design reliable and extensible classes with multiple inheritance. For more detail, see https://www.python.org/download/releases/2.3/mro/.

对于新式类，动态调整顺序是必要的，因为所有的多继承都会有一个或多个菱形关系(从最底部的类向上，至少会有一个父类可以通过多条路径访问到）。例如，所有新式类都继承自object，所以任何多继承都会有多条路径到达object。为了防止基类被重复访问，动态算法线性化搜索顺序，每个类都按从左到右的顺序特别指定了顺序，每个父类只调用一次，这是单调的（也就是说一个类被继承时不会影响它祖先的次序）。所有这些特性使得设计可靠并且可扩展的多继承类成为可能。有关详细信息，请参阅:

## 9.6. Private Variables and Class-local References(私有变量和类本地引用)
“Private” instance variables that cannot be accessed except from inside an object don’t exist in Python. However, there is a convention that is followed by most Python code: a name prefixed with an underscore (e.g. _spam) should be treated as a non-public part of the API (whether it is a function, a method or a data member). It should be considered an implementation detail and subject to change without notice.

在 Python 中不存在只能从对象内部访问的“私有”实例变量。然而，有一项大多数 Python 代码都遵循的公约：带有下划线`（例如_spam）`前缀的名称应被视为非公开的 API 的一部分（无论是函数、 方法还是数据成员）。它应该被当做一个实现细节，将来如果有变化恕不另行通知。

Since there is a valid use-case for class-private members (namely to avoid name clashes of names with names defined by subclasses), there is limited support for such a mechanism, called name mangling. Any identifier of the form `__spam` (at least two leading underscores, at most one trailing underscore) is textually replaced with _classname__spam, where classname is the current class name with leading underscore(s) stripped. This mangling is done without regard to the syntactic position of the identifier, as long as it occurs within the definition of a class.

因为有一个合理的类私有成员的使用场景（即为了避免名称与子类定义的名称冲突），Python 对这种机制有简单的支持，叫做name mangling。`__spam` 形式的任何标识符(前面至少两个下划线，后面至多一个下划线）将被替换为`_classname__spam，classname`是当前类的名字。此mangling会生效而不考虑该标识符的句法位置，只要它出现在类的定义的范围内。

Name mangling is helpful for letting subclasses override methods without breaking intraclass method calls.
Name mangling 有利于子类重写父类的方法而不会破坏类内部的方法调用
For example:
```
class Mapping:
    def __init__(self, iterable):
        self.items_list = []
        self.__update(iterable)

    def update(self, iterable):
        for item in iterable:
            self.items_list.append(item)

    __update = update   # private copy of original update() method

class MappingSubclass(Mapping):

    def update(self, keys, values):
        # provides new signature for update()
        # but does not break __init__()
        for item in zip(keys, values):
            self.items_list.append(item)
```
Note that the mangling rules are designed mostly to avoid accidents; it still is possible to access or modify a variable that is considered private. This can even be useful in special circumstances, such as in the debugger.

请注意 mangling 规则的目的主要是避免发生意外；它仍然有可能访问或修改一个被认为是私有的变量。这在特殊情况下，例如调试的时候，还是有用的。

Notice that code passed to exec, eval() or execfile() does not consider the classname of the invoking class to be the current class; this is similar to the effect of the global statement, the effect of which is likewise restricted to code that is byte-compiled together. The same restriction applies to getattr(), setattr() and delattr(), as well as when referencing __dict__ directly.

请注意传递给exec、 eval()或execfile()的代码没有考虑要将调用类的类名当作当前类；这类似于global语句的效果，影响只限于一起进行字节编译的代码。相同的限制适用于getattr()、 setattr()和delattr()，以及直接引用`__dict__`时.

## 9.7. Odds and Ends
User-defined exceptions are identified by classes as well. Using this mechanism it is possible to create extensible hierarchies of exceptions.

有时候类似于Pascal 的"record" 或 C 的"struct"的数据类型很有用，它们把几个已命名的数据项目绑定在一起。一个空的类定义可以很好地做到：
```
class Employee:
    pass

john = Employee() # Create an empty employee record

# Fill the fields of the record
john.name = 'John Doe'
john.dept = 'computer lab'
john.salary = 1000
```
A piece of Python code that expects a particular abstract data type can often be passed a class that emulates the methods of that data type instead. For instance, if you have a function that formats some data from a file object, you can define a class with methods read() and readline() that get the data from a string buffer instead, and pass it as an argument.

某一段Python代码需要一个特殊的抽象数据结构的话，通常可以传入一个类来模拟该数据类型的方法。例如，如果你有一个用于从文件对象中格式化数据的函数，你可以定义一个带有read ()和readline () 方法的类，以此从字符串缓冲读取数据，然后将该类的对象作为参数传入前述的函数。

## 9.8. Exceptions Are Classes Too(异常也是类)
User-defined exceptions are identified by classes as well. Using this mechanism it is possible to create extensible hierarchies of exceptions.

用户定义的异常类也由类标识。利用这个机制可以创建可扩展的异常层次

There are two new valid (semantic) forms for the raise statement:(raise语句有两种新的有效的（语义上的）形式)
```
raise Class, instance
raise instance
```
In the first form, instance must be an instance of Class or of a class derived from it. The second form is a shorthand for:

第一种形式中，instance必须是Class或者它的子类的实例。第二种形式是一种简写
```
raise instance.__class__, instance
```
A class in an except clause is compatible with an exception if it is the same class or a base class thereof (but not the other way around — an except clause listing a derived class is not compatible with a base class). For example, the following code will print B, C, D in that order:

except子句中的类如果与异常是同一个类或者是其基类，那么它们就是相容的（但是反过来是不行的——except子句列出的子类与基类是不相容的),例如,下面的代码将按该顺序打印B,C,D:

```
class B:
    pass
class C(B):
    pass
class D(C):
    pass

for c in [B, C, D]:
    try:
        raise c()
    except D:
        print "D"
    except C:
        print "C"
    except B:
        print "B"
```
Note that if the except clauses were reversed (with except B first), it would have printed B, B, B — the first matching except clause is triggered.

请注意，如果except 子句的顺序倒过来 (excpet B B在最前面），它就会打印B，B，B -- 第一个匹配的异常被触发.

When an error message is printed for an unhandled exception, the exception’s class name is printed, then a colon and a space, and finally the instance converted to a string using the built-in function str().

打印一个异常类的错误信息时，先打印类名，然后是一个空格、一个冒号，然后是用内置函数str()将类转换得到的完整字符串.








