## 9.Class

Compared with other programming languages, Python’s class mechanism(原理) adds classes with a minimum(最小的) of new syntax and (语义). It is a mixture(混合) of the class mechanisms(机制) found in C++ and Modula-3. Python classes provide all the standard features of Object Oriented Programming(面向对象编程的标准特点): 

the class inheritance mechanism allows multiple base classes, a derived class can override any methods of its base class or classes, and a method can call the method of a base class with the same name. Objects can contain arbitrary amounts and kinds of data. As is true for modules, classes partake of the dynamic nature of Python: they are created at runtime, and can be modified further after creation.  

类继承机制允许有多个基类，继承的类可以覆盖其基类或类的任何方法，方法能够以相同的名称调用基类中的方法。对象可以包含任意数量和种类的数据。和模块一样，类同样具有 Python 的动态性质：它们在运行时创建，并可以在创建之后进一步修改。

In C++ terminology, normally class members (including the data members) are public (except see below Private Variables and Class-local References), and all member functions are virtual. As in Modula-3, there are no shorthands for referencing the object’s members from its methods: the method function is declared with an explicit first argument representing the object, which is provided implicitly by the call. As in Smalltalk, classes themselves are objects. This provides semantics for importing and renaming. Unlike C++ and Modula-3, built-in types can be used as base classes for extension by the user. Also, like in C++, most built-in operators with special syntax (arithmetic operators, subscripting etc.) can be redefined for class instances.  

用 C++ 术语来讲，通常情况下类成员（包括数据成员）是公有的（其它情况见下文私有变量和类本地引用），所有的成员函数都是虚拟的。与Modula-3一样,
在成员方法中没有简便的方式引用对象的成员：方法函数的声明用显式的第一个参数表示对象本身，调用时会隐式地引用该对象。与 Smalltalk一样, 类本身也是对象。这给导入类和重命名类提供了语义上的合理性。与 C++ 和 Modula-3 不同，用户可以用内置类型作为基类进行扩展。此外，像 C++一样,，类实例可以重定义大多数带有特殊语法的内置操作符（算术运算符、 下标等）。

(Lacking universally accepted terminology to talk about classes, I will make occasional use of Smalltalk and C++ terms. I would use Modula-3 terms, since its object-oriented semantics are closer to those of Python than C++, but I expect that few readers have heard of it.)

## 9.1. A Word About Names and Objects(名称和对象)

Objects have individuality, and multiple names (in multiple scopes) can be bound to the same object. This is known as aliasing in other languages. This is usually not appreciated on a first glance at Python, and can be safely ignored when dealing with immutable basic types (numbers, strings, tuples). However, aliasing has a possibly surprising effect on the semantics of Python code involving mutable objects such as lists, dictionaries, and most other types. This is usually used to the benefit of the program, since aliases behave like pointers in some respects. For example, passing an object is cheap since only a pointer is passed by the implementation; and if a function modifies an object passed as an argument, the caller will see the change — this eliminates the need for two different argument passing mechanisms as in Pascal.  

对象是独立的，多个名字（在多个作用域中）可以绑定到同一个对象。这在其他语言中称为别名。第一次粗略浏览 Python时经常不会注意到这个特性， 而且处理不可变的基本类型（数字，字符串，元组）时忽略这一点也没什么问题。然而， 在Python 代码涉及可变对象如列表、 字典和大多数其它类型时，别名可能具有意想不到语义效果。这通常有助于优化程序，因为别名的行为在某些方面类似指针。例如，传递一个对象的开销是很小的，因为在实现上只是传递了一个指针；如果函数修改了作为参数传进来的对象，调用者也会看到变化 —— 这就避免了类似 Pascal 中需要两个不同参数的传递机制。

## 9.2. Python Scopes and Namespaces(作用域和命名空间)

Before introducing(介绍) classes, I first have to tell you something about Python’s scope rules. Class definitions play some neat tricks with namespace
(类的定义巧妙的运用了命名空间), and you need to know how scopes and namespaces work to fully understand what’s going on(了解工作原理). Incidentally(顺便提一下), knowledge about this subject is useful for any advanced Python programmer(对于任何高级程序员来讲都是非常有帮助的).

Let’s begin with some definitions.

A namespace is a mapping from names to objects. Most namespaces are currently implemented as Python dictionaries, but that’s normally not noticeable in any way (except for performance), and it may change in the future. Examples of namespaces are: the set of built-in names (containing functions such as abs(), and built-in exception names); the global names in a module; and the local names in a function invocation. In a sense the set of attributes of an object also form a namespace. The important thing to know about namespaces is that there is absolutely no relation between names in different namespaces; for instance, two different modules may both define a function maximize without confusion — users of the modules must prefix it with the module name.  

命名空间是从名称到对象的映射。当前命名空间主要是通过 Python 字典实现的，不过通常不会引起任何关注（除了性能方面），它以后也有可能会改变。以下有一些命名空间的例子：内置名称集（包括函数名例如abs()和内置异常的名称）；模块中的全局名称；函数调用中的局部名称。在某种意义上，对象的属性也形成一个命名空间。关于命名空间需要知道的重要一点是不同命名空间内的名称(如函数名)绝对没有任何关系；例如，两个不同模块可以都定义函数maximize而不会产生混淆 —— 模块的使用者必须以模块名为前缀引用它们.

By the way, I use the word attribute for any name following a dot — for example, in the expression z.real, real is an attribute of the object z. Strictly speaking, references to names in modules are attribute references: in the expression modname.funcname, modname is a module object and funcname is an attribute of it. In this case there happens to be a straightforward mapping between the module’s attributes and the global names defined in the module: they share the same namespace! [1]  

顺便说一句，我使用属性 这个词称呼 " . " 后面的任何名称 —— 例如，在表达式z.real中，real是z对象的一个属性。严格地说，对模块中的名称的引用是属性引用：在表达式modname.funcname中， modname是一个模块对象，funcname是它的一个属性。在这种情况下，模块的属性和模块中定义的全局名称之间碰巧是直接的映射：它们共享同一命名空间 ！[1]

Attributes may be read-only or writable. In the latter case, assignment to attributes is possible. Module attributes are writable: you can write modname.the_answer = 42. Writable attributes may also be deleted with the del statement. For example, del modname.the_answer will remove the attribute the_answer from the object named by modname.  

属性可以是只读的也可以是可写的。在后一种情况下，可以对属性赋值。模块的属性都是可写的：你可以这样写modname.the_answer=42.可写的属性也可以用del语句删除。
例如，del modname.the_answer将会删除对象modname中的the_answer属性。

Namespaces are created at different moments and have different lifetimes. The namespace containing the built-in names is created when the Python interpreter starts up, and is never deleted. The global namespace for a module is created when the module definition is read in; normally, module namespaces also last until the interpreter quits. The statements executed by the top-level invocation of the interpreter, either read from a script file or interactively, are considered part of a module called __main__, so they have their own global namespace. (The built-in names actually also live in a module; this is called __builtin__.)  

各个命名空间创建的时刻是不一样的，且有着不同的生命周期。包含内置名称的命名空间在 Python 解释器启动时创建，永远不会被删除。模块的全局命名空间在读入模块定义时创建；通常情况下，模块命名空间也会一直保存到解释器退出。在解释器最外层调用执行的语句，不管是从脚本文件中读入还是来自交互式输入，都被当作模块__main__的一部分，所以它们有它们自己的全局命名空间。(内置名称实际上也存在于一个模块中，这个模块叫__builtin__。)

The local namespace for a function is created when the function is called, and deleted when the function returns or raises an exception that is not handled within the function. (Actually, forgetting would be a better way to describe what actually happens.) Of course, recursive invocations each have their own local namespace.  
函数的局部命名空间在函数调用时创建，在函数返回或者引发了一个函数内部没有处理的异常时删除。（实际上，用遗忘来形容到底发生了什么更为贴切。)当然，每个递归调用有它们自己的局部命名空间。

A scope is a textual region of a Python program where a namespace is directly accessible. “Directly accessible” here means that an unqualified reference to a name attempts to find the name in the namespace. 

作用域:是Python 程序中可以直接访问一个命名空间的代码区域。这里的“直接访问”的意思是用没有前缀的引用在命名空间中找到的相应的名称。

Although scopes are determined statically, they are used dynamically. At any time during execution, there are at least three nested scopes whose namespaces are directly accessible:
* the innermost scope, which is searched first, contains the local names 
* the scopes of any enclosing functions, which are searched starting with the nearest enclosing scope, contains non-local, but also non-global names 
* the next-to-last scope contains the current module’s global names 
* the outermost scope (searched last) is the namespace containing built-in names 

虽然作用域的确定是静态的，但它们的使用是动态的。程序执行过程中的任何时候，至少有三个嵌套的作用域，它们的命名空间是可以直接访问的：

* 首先搜索最里面包含局部命名的作用域
* 其次搜索所有调用函数的作用域，从最内层调用函数的作用域开始，它们包含非局部但也非全局的命名
* 倒数第二个搜索的作用域是包含当前模块全局命名的作用域
* 最后搜索的作用域是最外面包含内置命名的命名空间

If a name is declared global, then all references and assignments go directly to the middle scope containing the module’s global names. Otherwise, all variables found outside of the innermost scope are read-only (an attempt to write to such a variable will simply create a new local variable in the innermost scope, leaving the identically named outer variable unchanged).

如果一个命名声明为全局的，那么对它的所有引用和赋值会直接搜索包含这个模块全局命名的作用域。否则，在最里面作用域之外找到的所有变量都是只读的（对这样的变量赋值会在最里面的作用域创建一个新 的局部变量，外部具有相同命名的那个变量不会改变）。

Usually, the local scope references the local names of the (textually) current function. Outside functions, the local scope references the same namespace as the global scope: the module’s namespace. Class definitions place yet another namespace in the local scope.

通常情况下，局部作用域引用当前函数的本地命名。函数之外，局部作用域引用的命名空间与全局作用域相同：模块的命名空间。类定义在局部命名空间中创建了另一个命名空间。

It is important to realize that scopes are determined textually: the global scope of a function defined in a module is that module’s namespace, no matter from where or by what alias the function is called. On the other hand, the actual search for names is done dynamically, at run time — however, the language definition is evolving towards static name resolution, at “compile” time, so don’t rely on dynamic name resolution! (In fact, local variables are already determined statically.)

认识到作用域是由代码确定的是非常重要的：函数的全局作用域是函数的定义所在的模块的命名空间，与函数调用的位置或者别名无关。另一方面，命名的实际搜索过程是动态的，在运行时确定的——然而，Python 语言也在不断发展，以后有可能会成为静态的“编译”时确定，所以不要依赖动态解析！（事实上，本地变量是已经确定静态。）

A special quirk of Python is that – if no global statement is in effect – assignments to names always go into the innermost scope. Assignments do not copy data — they just bind names to objects. The same is true for deletions: the statement del x removes the binding of x from the namespace referenced by the local scope. In fact, all operations that introduce new names use the local scope: in particular, import statements and function definitions bind the module or function name in the local scope. (The global statement can be used to indicate that particular variables live in the global scope.)

Python的一个特别之处在于——如果没有使用global语法——其赋值操作总是在最里层的作用域。赋值不会复制数据——只是将命名绑定到对象。删除也是如此：del x只是从局部作用域的命名空间中删除命名x。事实上，所有引入新命名的操作都作用于局部作用域： 特别是import语句和函数定义将模块名或函数绑定于局部作用域。(可以使用   Global 语句将变量引入到全局作用域。)


























