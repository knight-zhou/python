官方文档:
The website: https://docs.python.org/2/

中文翻译文档：
http://python.usyiyi.cn/translate/python_278/tutorial/classes.html

简明教程:
http://www.kuqin.com/abyteofpython_cn/index.html

第九章章节目录:

	9.1 名称和对象
	9.2 Python作用域和命名空间
	9.3 初识类
	9.4 补充说明
	9.5 继承
	9.6 私有变量和类本地引用
	9.7 零碎的摘要
	9.8 异常也是类
	9.9 迭代器
	9.10 生成器
	9.11 生成器表达式