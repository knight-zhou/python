## 9.9. Iterators(迭代器)
By now you have probably noticed that most container objects can be looped over using a for statement:

现在你可能注意到大多数容器对象都可以用for遍历：
```
for element in [1, 2, 3]:
    print element
for element in (1, 2, 3):
    print element
for key in {'one':1, 'two':2}:
    print key
for char in "123":
    print char
for line in open("myfile.txt"):
    print line,
```
This style of access is clear, concise, and convenient. The use of iterators pervades and unifies Python. Behind the scenes, the for statement calls iter() on the container object. The function returns an iterator object that defines the method next() which accesses elements in the container one at a time. When there are no more elements, next() raises a StopIteration exception which tells the for loop to terminate. This example shows how it all works:

这种风格的访问明确,简洁和方便.迭代器的用法在Python中普遍而且统一.在后台,for语句在容器对象上调用iter().该函数返回一个定义了next ()方法的迭代器对象，它在容器中逐一访问元素。没有后续的元素时， next()会引发StopIteration异常，告诉for循环终止。此示例显示它是如何工作:

```
>>> s = 'abc'
>>> it = iter(s)
>>> it
<iterator object at 0x00A1DB50>
>>> it.next()
'a'
>>> it.next()
'b'
>>> it.next()
'c'
>>> it.next()
Traceback (most recent call last):
  File "<stdin>", line 1, in ?
    it.next()
StopIteration
```
Having seen the mechanics behind the iterator protocol, it is easy to add iterator behavior to your classes. Define an __iter__() method which returns an object with a next() method. If the class defines next(), then `__iter__()` can just return self:

看过迭代器协议背后的机制后，将很容易将迭代器的行为添加到你的类中。定义一个`__iter__()`方法，它返回一个有next()方法的对象。如果类定义了`next()，__iter__()` 可以只返回self：

```
class Reverse:
    """Iterator for looping over a sequence backwards."""
    def __init__(self, data):
        self.data = data
        self.index = len(data)
    def __iter__(self):
        return self
    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]
```
```
>>> rev = Reverse('spam')
>>> iter(rev)
<__main__.Reverse object at 0x00A1DB50>
>>> for char in rev:
...     print char
...
m
a
p
s
```

## 9.10. Generators(生成器)
Generators are a simple and powerful tool for creating iterators. They are written like regular functions but use the yield statement whenever they want to return data. Each time next() is called on it, the generator resumes where it left off (it remembers all the data values and which statement was last executed). An example shows that generators can be trivially easy to create:

生成器是创建迭代器的一种简单而强大的工具。它们写起来就像是正规的函数，只是需要返回数据的时候使用yield语句。每次`next()`调用时，生成器再恢复它离开的位置（它记忆语句最后一次执行的位置和所有的数据值）。以下示例演示了生成器可以非常简单地创建出来：

```
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]
```
```        
>>> for char in reverse('golf'):
...     print char
...
f
l
o
g
```
Anything that can be done with generators can also be done with class-based iterators as described in the previous section. What makes generators so compact is that the __iter__() and next() methods are created automatically.

Another key feature is that the local variables and execution state are automatically saved between calls. This made the function easier to write and much more clear than an approach using instance variables like self.index and self.data.

In addition to automatic method creation and saving program state, when generators terminate, they automatically raise StopIteration. In combination, these features make it easy to create iterators with no more effort than writing a regular function.

生成器能做到的所有事，前一节所述的基于类的迭代器也能做到。生成器这么紧凑的原因是因为`__iter__()`和`next ()`方法是自动创建的。
另一个关键功能是调用时自动保存局部变量和执行状态。这使得该函数相比实例变量，如self.index和self.data方法，更容易写，更清楚地使用。
除了自动方法创建和保存的程序状态外，当创建完成，他们会自动抛出StopIteration。组合起来，这些功能使得创建迭代器如同编写函数般简单。

## 9.11. Generator Expressions(生成器表达式)
Some simple generators can be coded succinctly as expressions using a syntax similar to list comprehensions but with parentheses instead of brackets. These expressions are designed for situations where the generator is used right away by an enclosing function. Generator expressions are more compact but less versatile than full generator definitions and tend to be more memory friendly than equivalent list comprehensions.

使用类似列表表示式的语法，一些简单的生成器可以写成简洁的表达式，但是使用圆括号代替方括号。这些表达式用于生成器在封闭的函数中使用的情况。生成器表达式更紧凑但没有完整定义的生成器用途广泛，而且比同等的列表表示式消耗更少的内存。

Examples:
```
>>> sum(i*i for i in range(10))                 # sum of squares
285

>>> xvec = [10, 20, 30]
>>> yvec = [7, 5, 3]
>>> sum(x*y for x,y in zip(xvec, yvec))         # dot product
260

>>> from math import pi, sin
>>> sine_table = dict((x, sin(x*pi/180)) for x in range(0, 91))

>>> unique_words = set(word  for line in page  for word in line.split())

>>> valedictorian = max((student.gpa, student.name) for student in graduates)

>>> data = 'golf'
>>> list(data[i] for i in range(len(data)-1,-1,-1))
['f', 'l', 'o', 'g']
```
Footnotes(脚注)

[1] Except for one thing. Module objects have a secret read-only attribute called __dict__ which returns the dictionary used to implement the module’s namespace; the name __dict__ is an attribute but not a global name. Obviously, using this violates the abstraction of namespace implementation, and should be restricted to things like post-mortem debuggers. 

有一件事除外。模块对象具有一个隐藏的只读属性叫做`__dict__`，它返回用于实现模块命名空间的字典；名称`__dict__`是一个属性而不是一个全局的名称。很明显，使用它违反了命名空间实现的抽象，应该限制在类似事后调试这样的事情上。




