## 9.4. Random Remarks(零碎的摘要)
Data attributes override method attributes with the same name; to avoid accidental name conflicts, which may cause hard-to-find bugs in large programs, it is wise to use some kind of convention that minimizes the chance of conflicts. Possible conventions include capitalizing method names, prefixing data attribute names with a small unique string (perhaps just an underscore), or using verbs for methods and nouns for data attributes.

Data attributes may be referenced by methods as well as by ordinary users (“clients”) of an object. In other words, classes are not usable to implement pure abstract data types. In fact, nothing in Python makes it possible to enforce data hiding — it is all based upon convention. (On the other hand, the Python implementation, written in C, can completely hide implementation details and control access to an object if necessary; this can be used by extensions to Python written in C.)

Clients should use data attributes with care — clients may mess up invariants maintained by the methods by stamping on their data attributes. Note that clients may add data attributes of their own to an instance object without affecting the validity of the methods, as long as name conflicts are avoided — again, a naming convention can save a lot of headaches here.

There is no shorthand for referencing data attributes (or other methods!) from within methods. I find that this actually increases the readability of methods: there is no chance of confusing local variables and instance variables when glancing through a method.

Often, the first argument of a method is called self. This is nothing more than a convention: the name self has absolutely no special meaning to Python. Note, however, that by not following the convention your code may be less readable to other Python programmers, and it is also conceivable that a class browser program might be written that relies upon such a convention.

Any function object that is a class attribute defines a method for instances of that class. It is not necessary that the function definition is textually enclosed in the class definition: assigning a function object to a local variable in the class is also ok. For example:

数据属性会覆盖同名的方法属性；为了避免意外的命名冲突，这在大型程序中可能带来极难发现的 bug，使用一些约定来减少冲突的机会是明智的。可能的约定包括大写方法名称的首字母，使用一个小写的独特字符串（也许只是一个下划线）作为数据属性名称的前缀，或者方法使用动词而数据属性使用名词。

数据属性可以被方法引用，也可以由一个对象的普通用户（“客户端”）使用。换句话说，类是不能用来实现纯抽象数据类型。事实上，Python 中不可能强制隐藏数据——那全部基于约定.（另一方面，如果需要，使用C编写的Python实现可以完全隐藏实现细节并控制对象的访问；这可以用来通过C语言扩展Python。）

客户应该谨慎的使用数据属性——客户可能通过弄乱他们的数据属性而使那些由方法维护的常量变得混乱。注意：只要能避免冲突，客户可以向一个实例对象添加他们自己的数据属性，而不会影响方法的正确性——再次强调，命名约定可以避免很多麻烦。

从方法内部引用数据属性（或其他方法）并没有快捷方式。我觉得这实际上增加了方法的可读性：当浏览一个方法时，可以很容易区分出局部变量和实例变量。

通常，方法的第一个参数称为self。这仅仅是一个约定：名字self对 Python 而言绝对没有任何特殊含义。但是请注意：如果不遵循这个约定，对其他的 Python 程序员而言你的代码可读性就会变差，而且有些类 查看 器程序也可能是遵循此约定编写的。

`类属性的任何函数对象都为那个类的实例定义了一个方法。函数定义代码不一定非得定义在类中：也可以将一个函数对象赋值给类中的一个局部变量。`例如:

# Function defined outside the class
```
def f1(self, x, y):        #在类之外定义函数
    return min(x, x+y)

class C:
    f = f1          #类之外的函数对象f1赋值给类中的局部变量f
    def g(self):
        return 'hello world'
    h = g
```
Now f, g and h are all attributes of class C that refer to function objects, and consequently they are all methods of instances of C — h being exactly equivalent to g. Note that this practice usually only serves to confuse the reader of a program.

现在f、 g和h都是类C中引用函数对象的属性，因此它们都是C的实例的方法 —— h完全等同于g。请注意，这种做法通常只会使读者更加困惑。

Methods may call other methods by using method attributes of the self argument:

通过使用self参数的方法属性，方法可以调用其他方法：

```
class Bag:
    def __init__(self):
        self.data = []
    def add(self, x):
        self.data.append(x)
    def addtwice(self, x):
        self.add(x)
        self.add(x)
```
Methods may reference global names in the same way as ordinary functions. The global scope associated with a method is the module containing its definition. (A class is never used as a global scope.) While one rarely encounters a good reason for using global data in a method, there are many legitimate uses of the global scope: for one thing, functions and modules imported into the global scope can be used by methods, as well as functions and classes defined in it. Usually, the class containing the method is itself defined in this global scope, and in the next section we’ll find some good reasons why a method would want to reference its own class.

Each value is an object, and therefore has a class (also called its type). It is stored as `object.__class__`.

方法可以像普通函数那样引用全局命名。与方法关联的全局作用域是包含类定义的模块。（类本身永远不会做为全局作用域使用。）尽管很少有好的理由在方法中使用全局数据，全局作用域确有很多合法的用途：其一是方法可以调用导入全局作用域的函数和模块，也可以调用定义在其中的类和函数。通常，包含此方法的类也会定义在这个全局作用域，在下一节我们会了解为何一个方法要引用自己的类。

每个值都是一个对象，因此每个值都有一个类（也称为类型）。它存储为`object.__class__`。

下面我们来看一个实例:

```
#!/usr/bin/env python
#coding:utf8

class Person:
    '''Represents(代表) a person.'''
    population = 0   # population:居民  在这里population 属于Person类的变量

    def __init__(self, name):      # 类的初始化函数
        '''Initializes the person's data.'''
        self.name = name                    # name使用self赋值,属于对象的变量
        print '(初始化...... %s)' % self.name

        # When this person is created, he/she
        # adds to the population
        Person.population += 1    #当类对象建立，类中的population 的值就加1

    def __del__(self):                        # 对象不再被使用的时候调用
        '''I am dying.'''
        print '%s says bye.' % self.name

        Person.population -= 1

        if Person.population == 0:
            print 'I am the last one.'
        else:
            print 'There are still %d people left.' % Person.population

    def sayHi(self):
        '''Greeting by the person.

        Really, that's all it does.'''
        print 'Hi, 我的名字是: %s.' % self.name

    def howMany(self):
        '''Prints the current population.'''
        if Person.population == 1:
            print 'I am the only person here.'
        else:
            print 'We have %d persons here.' % Person.population

knight = Person('knight')

knight.sayHi()
# knight.howMany()

# Tom = Person('Abdul Tom')
# Tom.sayHi()
# Tom.howMany()

# knight.sayHi()
# knight.howMany()
```
这是一个很长的例子，但是它有助于说明类与对象的变量的本质。这里，population属于Person类，因此是一个类的变量。name变量属于对象（它使用self赋值）因此是对象的变量。

观察可以发现__init__方法用一个名字来初始化Person实例。在这个方法中，我们让population增加1，这是因为我们增加了一个人。同样可以发现，self.name的值根据每个对象指定，这表明了它作为对象的变量的本质。

记住，你只能使用self变量来参考同一个对象的变量和方法。这被称为 属性参考 。

在这个程序中，我们还看到docstring对于类和方法同样有用。我们可以在运行时使用Person.__doc__和Person.sayHi.__doc__来分别访问类与方法的文档字符串。

就如同__init__方法一样，还有一个特殊的方法__del__，它在对象消逝的时候被调用。对象消逝即对象不再被使用，它所占用的内存将返回给系统作它用。在这个方法里面，我们只是简单地把Person.population减1。

当对象不再被使用时，__del__方法运行，但是很难保证这个方法究竟在 什么时候 运行。如果你想要指明它的运行，你就得使用del语句，就如同我们在以前的例子中使用的那样。

