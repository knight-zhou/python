## logging模块
通过和开发的沟通，得知logging模块是一个半约定俗称的东西,能够方便的管理日志

比如我们可以logging模块定义日志的格式，日志的存储方法(是写到数据库 还是打印到终端还是写到文件，还是同时记录)等.

当然logging模块能实现的东西可以用其他实现,比如写入文件可以 `f=open(....)`,写入数据库可以定义写入数据库的文件.

但是通过logging模块非常的灵活也便于管理.

注意:logging模块 写入的日志的内容就是程序已经取到的值，并不是logging.info就过滤出info信息，logging.error就是过滤error信息.

### 写入日志的方法
* 可以通过截取到error的内容然后通过logging.error('error的内容') 写入，通过logging.info('取到的info内容写入')
* 也可以通过捕获异常能看写入日志, 捕获到错误就logging.error('')写入错误日志文件,其他的就写入info
* 可以通过if ... else...来写入日志,如果if == '404'  就记录longging.error(''),其他就写入info.

## 示例
```
#coding:utf8
import logging

#创建一个logger
logger = logging.getLogger('mylogger')
#设置日志的级别
logger.setLevel(logging.DEBUG)

#创建一个handler,用于写入日志文件
fh=logging.FileHandler('test.log')
fh.setLevel(logging.DEBUG)    #debug日志级别写入文件
#定义handler的输出格式
fomatter= logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

#写入文件以这种格式写入
fh.setFormatter(fomatter)


#给logger添加handler
logger.addHandler(fh)

#记录一条日志
logger.info('football')


```
## 解释说明:
```
结合上面的例子，我们说下几个最常使用的API：

(1) logging.getLogger([name])
返回一个logger实例，如果没有指定name，返回root logger。只要name相同，返回的logger实例都是同一个而且只有一个，即name和logger实例是一一对应的。这意味着，无需把logger实例在各个模块中传递。只要知道name，就能得到同一个logger实例。
　　
(2) Logger.setLevel(lvl)
　　
设置logger的level， level有以下几个级别：
级别高低顺序：NOTSET < DEBUG < INFO < WARNING < ERROR < CRITICAL
如果把looger的级别设置为INFO， 那么小于INFO级别的日志都不输出， 大于等于INFO级别的日志都输出　


(3)Logger.addHandler(hdlr)

通过handler对象可以把日志内容写到不同的地方,python提供了十几种实用handler，比较常用有：

StreamHandler: 输出到控制台
FileHandler:   输出到文件
　
```


