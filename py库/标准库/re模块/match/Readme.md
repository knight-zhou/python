## Python通过re模块提供对正则表达式的支持。

使用re的一般步骤:

是先使用re.compile()函数，将正则表达式的字符串形式编译为Pattern实例，然后使用Pattern实例处理文本并获得匹配结果（一个Match实例），最后使用Match实例获得信息，进行其他的操作。

举一个简单的例子，在寻找一个字符串中所有的英文字符：
v12.py


一旦你有了已经编译了的正则表达式的对象，你要用它做什么呢？`RegexObject` 实例有一些方法和属性.下面显示了最重要的几个:

| 方法/属性   |作用        								      |
| ------------|:---------------------------------------------:|
| match()	  |决定 RE 是否在字符串刚开始的位置匹配           |
| search()    |扫描字符串,找到这个 RE 匹配的位置              |
| findall()   |找到RE匹配的所有子串,并把它们作为一个列表返回  |
| finditer()  |找到RE匹配的所有子串,并把它们作为一个迭代器返回|

如果没有匹配到的话，match()和search()将返回 None.如果成功的话,就会返回一个`MatchObject` 实例，其中有这次匹配的信息：它是从哪里开始和结束，它所匹配的子串等等.

现在，让我们试着用它来匹配一个字符串，如 "tempo"。这时，match() 将返回一个 MatchObject。因此你可以将结果保存在变量里以便後面使用
```
import re
p = re.compile('[a-z]+')
m = p.match('temp999')
print m

```
现在你可以查询`MatchObject`关于匹配字符串的相关信息了.MatchObject 实例也有几个方法和属性;最重要的那些如下所示:

| 方法/属性   |作用        					            |
| ------------|:---------------------------------------:|
| group()	  |返回被 RE 匹配的字符串           	    |
| start()     |返回匹配开始的位置                       |
| end()       |返回匹配结束的位置                       |
| span()      |找返回一个元组包含匹配(开始,结束)的位置  |

试一试:
```
print m.group()
print m.start(),m.end()
print m.span()
```
group() 返回 RE 匹配的子串。start() 和 end() 返回匹配开始和结束时的索引。span() 则用单个元组把开始和结束时的索引一起返回.

在实际程序中，最常见的作法是将 MatchObject 保存在一个变量里，然後检查它是否为 None，通常如下所示：
```
import re
p = re.compile('[a-z]+')
m = p.match('temp99888')
print m
if m:
    print 'Match found:',m.group()
else:
    print 'No Match'
```
