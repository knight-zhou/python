findall
re.findall(pattern, string[, flags])
findall(string[, pos[, endpos]])

作用：在字符串中找到正则表达式所匹配的所有子串，并组成一个列表返回


findall和finditer的区别:

findall(): 找到RE匹配的所有子串，并把它们作为一个列表返回
finditer():找到RE匹配的所有子串，并把它们作为一个迭代器返回


