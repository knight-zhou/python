#coding:utf8
#使用这个模块的函数前先确保操作的列表是已排序的
import bisect
scores = [(100, 'perl'), (200, 'tcl'), (400, 'lua'),(500, 'python')]
# scores.append((300,'ruby'))
print "原来的列表:",scores
bisect.insort(scores,(300,'ruby'))

print "处理后的列表:",scores
# print "查看属性:",dir(bisect)

#########################################
aa = [1,4,6,7]
bisect.insort(aa,3)
print aa