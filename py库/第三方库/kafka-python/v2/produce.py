#coding:utf-8
from kafka import KafkaProducer
import json
'''
https://pypi.python.org/pypi/kafka-python
''''
producer = KafkaProducer(bootstrap_servers='192.168.30.36:9092')

#批量生产消息
# for _ in range(100):
#      producer.send('news', b'7766')

#单独生产消息,news是topic
future = producer.send('news', b'uuu')
result = future.get(timeout=60)
producer.flush()

producer.send('news',key=b'name',value=b'knight')
