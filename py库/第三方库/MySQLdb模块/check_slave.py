#!/usr/bin/env python
#coding:utf-8

import  MySQLdb
db = MySQLdb.connect(host='localhost',port = 3306,user='root',read_default_file='/etc/my.cnf',passwd='root')
cursor = db.cursor()
sql = "show slave status"
cursor.execute(sql)
results = cursor.fetchall()


s_io=results[0][10]
s_sql=results[0][11]

if s_io=='Yes' and s_sql=='Yes':
    print 2
elif s_io=='Yes' or s_sql=='Yes':
    print 1
else:
    print 0

